<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Article" itemid="{{ get_permalink() }}">
   <head>

		 @include('header')

      <link rel="stylesheet" type="text/css" href="http://crtable.win/wp-content/themes/holder/m.css" media="screen"/>
      <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet" type="text/css">

 			@include('css')

   </head>
   <body>
      <div id="n">
         <div class="bb">
            <h2 style="padding-top:8px;"><a href="{{ home_url() }}" title="Homepage" style="color:white;">{{ sitename() }}</a></h2>
         </div>
      </div>

      <!-- google_ad_section_end -->
      <div id="b">

				 @yield('content')

      </div>

      <div id="s">
         <div class="f">

					 @foreach( config('themes.page') as $page )
							<a href="{{ permalink( $page, 'page') }}">{{ ucwords($page) }}</a> |
					 @endforeach
					<a href="{{ home_url('sitemap.xml') }}">Sitemap</a>


            <blockquote><a>While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy.</a></blockquote>
            <br/>
            <blockquote><a>© Copyright {{ date('Y') }} {{ sitename() }}. All Rights Reserved.</a></blockquote>
         </div>
         <div id="bt"><a href="#top" rel="nofollow"><span></span>Back to top</a></div>
      </div>

@include('footer')

   </body>
</html>
