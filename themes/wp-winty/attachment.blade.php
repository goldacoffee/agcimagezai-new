@extends('layout')

@section('content')

<div id="a">

	 <div class="at tc">
			{!! ads('responsive') !!}
	 </div>

	 <div class="bc">
     <a href="{{ home_url() }}" rel="nofollow">Home</a><i>&#9002;</i>
     <a href="{{ permalink( $query ) }}">{{ $query }}</a><i>&#9002;</i>
		 <a href="{{ get_permalink() }}">{{ $subquery }}</a><i>&#9002;</i>
     <a rel="nofollow">{{ $subquery }}</a>
  </div>

	 <div class="ac">
			{!! ads('responsive') !!}
	 </div>

	 <div class="ii t">
			<h1>{{ $subquery }}</h1>
			<div class="ia">
				<a class="z" href="#" title="{{ $gallery['current']['title'] }}">
					<img style="width:100%" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}" />
				</a>
			</div>
			<div class="c"></div>
	 </div>
	 <div class="c"></div>
	 <div class="al">
			<a class="pn m18 k" href="{{ attachment_url($query, $gallery['prev']['title'] ) }}" title="Previous">&#10218; Previous</a>

			<a class="nn m18 t" href="{{ attachment_url($query, $gallery['next']['title'] ) }}" title="Next">Next &#10219;</a><br/>

			<hr/>
			<h3 class="rl tc">Description for {{ $subquery }}</h3>
			<p>Uploaded by: {{ sitename() }}<br/>Resolution: {{ $gallery['current']['size'] }}<br/>
				Uploaded at: {{ date('d/m/Y') }}<br/>
			<blockquote>Make sure you have no surprises. Ask your real estate agent for a list of permits necessary to build your dream home on the lot you chose. If they will not provide this, then go to your city and county and get the list. Make sure your construction contactor provides all of these permits in their contract with you.</blockquote>
			<br/>
			<div class="c"></div>
	 </div>
	 <hr/>
	 <div class="at tc">
			 {!! ads('responsive') !!}
	 </div>
	 <h5>Gallery of {{ $subquery }}</h5>
	 <div id="cg">

		 @foreach( $results as $key => $item )
			<div class="tb">
				 <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}">

						<img onerror="this.onerror=null;this.src='{{ $item['small'] }}';" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" width="205" height="205"/>

				 </a>
				 <div class="c"></div>
				 <h2 class="it tc">{{ $item['title'] }}</h2>
			</div>
			@endforeach

			<div class="c"></div>
	 </div>

	 <div class="c"></div>
</div>
<div class="c"></div>


@endsection
