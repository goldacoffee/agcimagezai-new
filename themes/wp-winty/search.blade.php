@extends('layout')

@section('content')

<div id="cn">
  <h1>{{ $query }} [ {{ count($results) }} Styles ]</h1>

   <div class="bc">
     <a href="{{ home_url() }}" rel="nofollow">Home</a><i>&#9002;</i>
     <a href="{{ get_permalink() }}">{{ $query }}</a><i>&#9002;</i>
     <a rel="nofollow">Currently viewing : {{ $query }}</a>
  </div>

   <div class="ha m18 tc">
      {!! ads('responsive') !!}
   </div>

   <div class="jc">
      <div class="sk k">
         <div class="is">
            <a class="z" href="#" title="{{ $results[0]['title'] }}">
              <img style="width:100%" src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}" />
            </a>
            <div class="c"></div>
            <h3 class="it">{{ $results[0]['title'] }}</h3>
         </div>
      </div>
      <div class="tk t">
         <h3>{{ $results[0]['title'] }}</h3>
         <p>Uploaded by: {{ sitename() }}<br/>Resolution: {{ $results[0]['size'] }}<br/>
           Uploaded at: {{ date('d/m/Y') }}<br/>
         <hr>
         <br>
      </div>
      <div class="c"></div>
      <hr>
      <blockquote>
         <p>
            @include('spintax/search-1')
         </p>
      </blockquote>

   </div>

   <hr/>

   <div class="at tc">
       {!! ads('responsive') !!}
   </div>

   <h5>Gallery of {{ $query }}</h5>
   <div id="cg">

     @foreach( $results as $key => $item )
      <div class="tb">
         <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}">

            <img onerror="this.onerror=null;this.src='{{ $item['small'] }}';" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" width="205" height="205"/>

         </a>
         <div class="c"></div>
         <h2 class="it tc">{{ $item['title'] }}</h2>
      </div>
      @endforeach

      <div class="c"></div>
   </div>

   <div class="c"></div>
</div>
<div class="c"></div>

@endsection
