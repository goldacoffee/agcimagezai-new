<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:og="//opengraphprotocol.org/schema/" xmlns:fb="//www.facebook.com/2008/fbml">
   <head>
		 	@include('header')
      @include('css')
   </head>

   <body>
      <div id="head">
         <div class="header">
            <a href="{{ home_url() }}" title="{{ sitename() }} Homepage">{{ sitename() }}</a>
            <div class="notice">
							<i>All of the pictures on this website was taken from source that we believe as "Public Domain", If you want to claim your image please <a href="{{ permalink('contact', 'page') }}" rel="nofollow" title="Contact Us">Contact Us</a></i>
						</div>
            <div class="clear"></div>
         </div>
      </div>
      <div id="container">

         <br/>

				 @yield('content')

				 @if( !is_page() )
         		@include('sidebar')
				 @endif
      </div>

<div class="clear"></div>

      <div id="footer">
         <div class="footers">
            <center>
               <p>
								 <a href="{{ home_url() }}" title="Home">Home</a> |
								 @foreach( config('themes.page') as $page )
								 	<a href="{{ permalink( $page, 'page' ) }}" title="{{ $page }}" rel="nofollow">{{ ucwords($page) }}</a> |
								 @endforeach
								 	<a target="_blank" href="{{ home_url('sitemap.xml') }}">Sitemap</a>
							 </p>
               <p>&copy; {{ date('Y') }} {{ sitename() }} All Rights Reserved &#9829;</p>
            </center>
         </div>
      </div>

		@include('footer')

   </body>
</html>
