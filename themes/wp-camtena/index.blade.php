@extends('layout')

@section('content')


<h1>{{ config('site.description') }}</h1>
<div id="content">

@foreach( $results as $key => $item)
   <div class="posting">
      <article id="post-16422" class="box-post post-16422 post type-post status-publish format-standard has-post-thumbnail hentry">

         <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}" rel="bookmark">
           <img width="241" height="150" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
         </a>

         <div class="ttlposting">
            <h2 class="entry-title">
              <a href="{{ permalink($query) }}" rel="bookmark" title="{{ strtolower($query) }} - {{ $item['title'] }}"><strong>{{ strtolower($query) }} - {{ $item['title'] }}</strong></a>
            </h2>
         </div>

         <div class="meta-post" style="display:none;">
           <span class="entry-author vcard">By <a class="url fn n" href="{{ home_url() }}" title="View all posts by {{ sitename() }}" rel="author">{{ sitename() }}</a></span>
           <span class="entry-date updated">On <a href="{{ permalink($query) }}" title="{{ date('d/m/Y') }}" rel="bookmark"><time datetime="{{ date('d/m/Y') }}" pubdate>{{ date('d/m/Y') }}</time></a></span>
         </div>

      </article>
   </div>
@endforeach

   <br/>
   <div class="clear"></div>

   @php
 	  $pagination = range(1,20)
 	 @endphp

   <div class ="page_navigation">
      <div class="wp-pagenavi">

        @foreach($pagination as $n => $page)
           @if( input()->get('page') == $n+1 )
               <strong class='current'>{{ $page }}</strong>
           @elseif( $page == end($pagination) )
               <a class="page" href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
           @else
               <a class="page" href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
           @endif
        @endforeach

      </div>
   </div>
</div>

@endsection
