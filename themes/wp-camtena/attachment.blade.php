@extends('layout')

@section('content')

<div id="content">

  @include('breadcrumb')

   <div class="single">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
         <meta itemscope="" itemprop="mainEntityOfPage" itemtype="https://schema.org/WebPage" itemid="{{ get_permalink() }}">
         <h1 itemprop="headline">{{ $subquery }}</h1>

         <div class="adspace">
            {!! ads('responsive') !!}
         </div>

         <a href="{{ attachment_url( $query, $gallery['current']['title'] ) }}">
           <center><img style="width:100%" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" title="{{ strtolower($query) }} {{ $gallery['current']['title'] }}" alt="{{ strtolower($query) }} {{ $gallery['current']['title'] }}"></center>
         </a>

         <center>
            <h2>{{ $gallery['current']['title'] }}</h2>
         </center>

           {!! ads('responsive') !!}

					 <nav>

				     <span class="nav-previous1">
				       <a href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}" rel="prev"><span class="meta-nav"><span class=" icon-double-angle-left"></span></span>«« PREV</a>
				     </span>

				     <span class="nav-next1">
				       <a href="{{ attachment_url( $query, $gallery['next']['title'] ) }}" rel="next">NEXT »» <span class="meta-nav"><span class=" icon-double-angle-right"></span></span></a>
				     </span>

				   </nav>

         <div itemprop="articleBody">
            <p><strong>{{ $query }}</strong></p>
            <p>
              @include('spintax/search-3')
            </p>
         </div>

         <p><em>{{ $subquery }}</em></p>

         <div style="display:none;">
            <div itemprop="image" itemscope="" itemtype="https://schema.org/ImageObject">
               <meta itemprop="url" content="{{ $gallery['current']['url'] }}">
               <meta itemprop="width" content="1500">
               <meta itemprop="height" content="598">
            </div>
         </div>

         <div style="display:none;">
            <div itemprop="aggregateRating" itemscope="" itemtype="schema.org/AggregateRating">
              <span itemprop="ratingValue">{{ rand(10,900) }}</span>
              <span itemprop="bestRating">{{ rand(10,900) }}</span>
              <span itemprop="ratingCount">{{ rand(10,900) }}</span>
            </div>
            <div itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
               <div itemprop="logo" itemscope="" itemtype="https://schema.org/ImageObject">
                  <meta itemprop="url" content="{{ $gallery['current']['url'] }}">
                  <meta itemprop="width" content="48">
                  <meta itemprop="height" content="48">
               </div>
               <meta itemprop="name" content="{{ sitename() }}">
            </div>
            <meta itemprop="datePublished" content="{{ date('d/m/Y') }}">
            <meta itemprop="dateModified" content="{{ date('d/m/Y') }}">
            <meta itemprop="author" content="{{ sitename() }}">
         </div>
         <br>
      </article>
   </div>
   <div class="clear"></div>
   <div class="sosmed">
     <span><a class="twitter" href="http://twitter.com/home?status=Reading: {{ get_permalink() }}" title="Share this post on Twitter!" target="_blank">Twitter</a></span>
     <span><a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u={{ get_permalink() }}&amp;t={{ $query }}" title="Share this post on Facebook!">Facebook</a></span>
     <span><a class="pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url={{ get_permalink() }}&amp;media=&amp;description={{ $query }}" title="Share this post on Pinterest!">Pinterest</a></span>
     <span><a class="google" target="_blank" href="https://plus.google.com/share?url={{ get_permalink() }}" title="Share this post on Google!">Google+</a></span>
   </div>
   <div style="clear"></div>
   <div class="title">
      <h3>Gallery of {{ $query }} {{ $subquery }}</h3>
      <br>
      <center>

    @foreach($results as $key => $item)
        <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}" rel="bookmark">

          <img width="1000" height="823" class="attachment-full size-full" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
        </a>
    @endforeach




    </center>
   </div>

   <div style="clear:both;margin-bottom:20px;"></div>



   <br>
   <div class="clear"></div>

</div>

@endsection
