<div id="sidebar">

   <br/>

   {!! ads('responsive') !!}

   <div class="clear"></div>


   <div class="sidebarbox">
      <h3 class="sidebarname">Recent Posts</h3>
      <div class="sidebarpopular">

        @foreach( array_slice($random_terms, 0, 100) as $term )
         <div class="sidebarpop">
            <div class="sidebarinfo">
               <a href="{{ permalink( $term ) }}" title="{{ $term }}">
                  <h3>{{ $term }}</h3>
               </a>
            </div>
         </div>
        @endforeach

      </div>
   </div>
</div>
<div class="clear"></div>
