	<div class="breadcrumb-wrap" xmlns:v="http://rdf.data-vocabulary.org/#">
		<ol class="breadcrumb btn-box">
		<li><span typeof="v:Breadcrumb"><a href="{{ home_url() }}" rel="v:url" property="v:title">Home</a></span></li>
		@if( is_home() )
				<li><span typeof="v:Breadcrumb">{{ config('site.description') }}</span></li>
		@elseif( is_page() )
			<li><span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $page_title }}</a></span></li>
			<li><span typeof="v:Breadcrumb">{{ $page_title }}</span>
		@elseif( is_search() )
			<li><span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $query }}</a></span></li>
			<li><span typeof="v:Breadcrumb">{{ $query }}</span>
		@elseif( is_attachment() )
			<li><span typeof="v:Breadcrumb"><a href="{{ permalink($query) }}" rel="v:url" property="v:title">{{ $query }}</a></span></li>
			<li><span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $subquery }}</a></span></li>
			<li><span typeof="v:Breadcrumb">{{ $subquery }}</span></li>
		@endif
		</ol>
	</div>
