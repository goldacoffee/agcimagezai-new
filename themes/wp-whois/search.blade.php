@extends('layout')

@section('content')

<div class="ktz-titlepage">
	<h1 class="entry-title clearfix">{{ $query }}</h1>
</div>


		<div class="metasingle-aftertitle">
			<div class="ktz-inner-metasingle">
				<span class="entry-author vcard">By <a class="url fn" href="{{ home_url() }}" title="{{ sitename() }}" rel="author">{{ sitename() }}</a></span>

				<span class="entry-date updated">On <a href="{{ get_permalink() }}" title="7:04 AM" rel="bookmark">
					<time datetime="{{ date('d/m/y') }}" pubdate>{{ date('d/m/y') }}</time>
				</a>
				</span>

				</div>
		</div>

		<div class="entry-content ktz-wrap-content-single clearfix ktz-post">


<img style="width:100%" src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" title="{{ $query }} {{ $results[0]['title'] }}" alt="{{ $query }} {{ $results[0]['title'] }}" />


<div class="ktz-bannersingletop">
{!! ads('responsive') !!}
</div>

  @include('spintax/search-1')
	
		<h4 class="related-title">Gallery of {{ $query }}</h4>

		<div class="ktz-gallery">
			<ul>

				@foreach($results as $key => $item)

				<li>
					<a href="{{ attachment_url($query, $item['title']) }}" title="{{ $query }} by {{ ucwords($item['title']) }}">
						<img style="height:140px;width:200px" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" title="{{ $query }} by {{ ucwords($item['title']) }}" alt="{{ $query }} by {{ ucwords($item['title']) }}" />
					</a>
				</li>

				@endforeach



			<ul>
		</div>



		<p class="ktz-tagcontent">Tags:

			@foreach( array_slice( $random_terms, 0, 5) as $term )
				<a href="{{ permalink( $term ) }}" title="{{ ucwords( $term ) }}" rel="bookmark">#{{ ucwords( $term ) }}</a>
			@endforeach


			</p>




</div>







@endsection
