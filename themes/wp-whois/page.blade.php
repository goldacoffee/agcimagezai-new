@extends('layout')

@section('content')

<div class="ktz-titlepage"><h1 class="entry-title clearfix">{{ $page_title }} - {{ sitename() }}</h1></div>

		
		
		<div class="entry-content ktz-wrap-content-single clearfix ktz-post">
							
@include('page/' . strtolower($page_title) )
		
		
		
		
</div>		
		
		
		
		
		


@endsection
