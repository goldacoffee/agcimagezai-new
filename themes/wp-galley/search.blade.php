@extends('layout')

@section('content')
      <div id="header" class="w pr mb18">
         <div class="w1 m0 pr">
            <h2 class="mb0"><a href="{{ home_url() }}" style="color:#ff4f00">{{ sitename() }}: {{ $query }}.</a></h2>
         </div>
      </div>

      <div class="w1 m0 cl">
         <div class="tc mb18 h9 ad">
			{!! ads('responsive') !!}
         </div>
         <div class="w2 oh mb18 cl">

	@include('breadcrumb')

            <h1 itemprop="headline">{{ $query }} - {{ $results[0]['title'] }}</h1>
            <div class="cl">

					@foreach($results as $key => $item)
							@if( $key < 4 )
							   <div class="post oh l mb18 bw bs pr id-15185" itemid="{{ $item['url'] }}" itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
							   <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $query }} - {{ $item['title'] }}">
									<img onerror="this.onerror=null;this.src='{{ $item['small'] }}';" data-src="{{ $item['url'] }}" src="{{ $item['small'] }}" alt="{{ $query }} - {{ $item['title'] }}" title="{{ $query }} - {{ $item['title'] }}" width="188" height="188" itemprop="contentURL">
							   </a>
							   <i class="pa ll">
								   <a target="_blank" title="{{ $item['title'] }}" href="{{ $item['small'] }}">S</a>
								   <a target="_blank" title="{{ $item['title'] }}" href="{{ $item['url'] }}">L</a>
							   </i>
							   </div>
							@endif
					@endforeach

   <div style="clear:both"></div>
         <div class="tc mb18 h9 ad">
			@yield('ads')
         </div>

	@foreach($results as $key => $item)

	   <div class="post oh l mb18 bw bs pr id-15185" itemid="{{ $item['url'] }}" itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
	   <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}">
			<img onerror="this.onerror=null;this.src='{{ $item['small'] }}';" data-src="{{ $item['url'] }}" src="{{ $item['small'] }}" alt="{{ $query }} - {{ $item['title'] }}" title="{{ $query }} - {{ $item['title'] }}" width="188" height="188" itemprop="contentURL">
	   </a>
	   <i class="pa ll">
		   <a target="_blank" title="{{ $item['title'] }}" href="{{ $item['small'] }}">S</a>
		   <a target="_blank" title="{{ $item['title'] }}" href="{{ $item['url'] }}">L</a>
	   </i>
	   </div>

   @endforeach

            </div>
         </div>
         <div class="tc mb18 h9 ad">
			{!! ads('responsive') !!}
         </div>

		 <div class="cl w2 oh">
		 <h2>{{ $query }}</h2>
		 @foreach($random_terms as $term)
			<a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }} &bullet; </a>
		 @endforeach
		 </div>
      </div>

@endsection
