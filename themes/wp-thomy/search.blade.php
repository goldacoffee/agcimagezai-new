@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('content')

		<div class="content">

			   @include('breadcrumb')

               <h1 class="post_title">{{ $query }}</h1>

               <div class='ads'>
					@yield('ads')
               </div>
               <div class="post_img">
					  <a href="{{ attachment_url( $query, $results[0]['title'] ) }}" rel="bookmark" title="{{ $results[0]['title'] }}">
						<img width="431" height="277" src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';"  class="attachment-full size-full" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}"/>
					  </a>
                  <h2>{{ $query }}</h2>
               </div>
					@yield('ads')
               <div class="cl"></div>
                <p>
					  <strong>{{ $query }}</strong> {{ spin('{have some|involve some}') }} pictures that related {{ spin('{each other|one another}') }}. Find out {{ spin('{the most recent|the newest}') }} pictures of {{ $query }} here, {{ spin('{and also you|so you}') }} can {{ spin('{get the|obtain the|have the|receive the|find the}') }} picture here simply. {{ $query }} picture {{ spin('{posted|published|submitted|placed|put up|uploaded}') }} ang {{ spin('{uploaded|published|submitted}') }} by Admin that {{ spin('{saved|preserved|kept}') }} {{ spin('{in our|inside our}') }} collection.
					  <br>
					  <strong>{{ $query }}</strong> have {{ spin('{an image|a graphic}') }} {{ spin('{associated with the|from the}') }} other.
					  <br>
					  <strong>{{ $query }}</strong> {{ spin('{It also|In addition, it}') }} will {{ spin('{feature a|include a}') }} picture of {{ spin('{a kind|a sort}') }} {{ spin('{that could be|that may be|that might be}') }} {{ spin('{seen in|observed in}') }} the gallery of {{ $query }}. The collection that {{ spin('{consisting of|comprising}') }} chosen picture and the best {{ spin('{among others|amongst others}') }}.
					  <br>
					  {{ spin('{These are|They are}') }} so many great picture list that {{ spin('{may become|could become}') }} your {{ spin('{inspiration|motivation|ideas|creativity|enthusiasm}') }} and informational {{ spin('{purpose of|reason for}') }} <em>{{ $query }}</em> design ideas {{ spin('{for your own|on your own}') }} collections. {{ spin('{we hope|hopefully|really is endless}') }} you {{ spin('{are all|are}') }} enjoy {{ spin('{and finally|and lastly}') }} {{ spin('{can find|will get}') }} the best picture from our collection that {{ spin('{posted|published|submitted|placed|put up|uploaded}') }} here and also use for {{ spin('{suitable|appropriate|ideal|suited}') }} needs for personal use. The brucall.com team also {{ spin('{provides the|supplies the}') }} picture in {{ spin('{High Quality|TOP QUALITY}') }} Resolution (HD {{ spin('{Resolution|Quality|Image resolution}') }}) {{ spin('{that can be|that may be}') }} downloaded {{ spin('{by simply|simply by}') }} way.
					  <br><br>
					  {{ spin('{You just|You merely}') }} have to {{ spin('{click on the|go through the}') }} gallery below the <u>{{ $query }}</u> picture. {{ spin('{We provide|We offer}') }} image <strong>{{ $query }}</strong> {{ spin('{is similar|is comparable}') }}, because our website {{ spin('{focus on|concentrate on|give attention to}') }} this category, users can {{ spin('{navigate|get around|understand|find their way}') }} easily and we show {{ spin('{a simple|a straightforward}') }} theme {{ spin('{to search for|to find}') }} images that allow a {{ spin('{user|consumer|customer|end user|individual}') }} {{ spin('{to search|to find}') }}, if your pictures are on our website and want to complain, you can {{ spin('{file|document|record}') }} a {{ spin('{complaint|problem|issue|grievance}') }} by sending {{ spin('{an email|a contact}') }} {{ spin('{is available|can be obtained|can be found|is offered|is obtainable|can be acquired}') }}. The {{ spin('{collection of|assortment of}') }} images <em>{{ $query }}</em> that are elected {{ spin('{directly|straight|immediately}') }} by the admin and with {{ spin('{high resolution|high res}') }} (HD) as well as facilitated to download images.
					  <br>
					  The picture with high reolusi will {{ spin('{facilitate|help|assist in|aid|help in|accomplish}') }} you in see and {{ spin('{observe|notice|see|watch|view|monitor}') }} our image collections
					  image provided by {{ sitename() }} team also provides {{ spin('{interior design|home design}') }} and {{ spin('{exterior|outside|external|outdoor|external surfaces}') }} home, to {{ spin('{be able to|have the ability to}') }} see {{ spin('{directly|straight|immediately}') }}, you {{ spin('{can use|may use}') }} the category navigation or {{ spin('{it could be|maybe it is}') }} using a {{ spin('{random|arbitrary}') }} post of {{ $query }}.
					  <br>
					  We hope {{ spin('{you enjoy|you love}') }} {{ spin('{and find|and discover}') }} one {{ spin('{of our|in our|of our own|of the|of your}') }} best {{ spin('{collection of|assortment of}') }} pictures and get {{ spin('{inspired|influenced|motivated|encouraged}') }} to {{ spin('{decorate|beautify|enhance}') }} your residence.
					  <br>
					  If {{ spin('{the link|the hyperlink}') }} is {{ spin('{broken|damaged|busted|cracked|shattered|destroyed}') }} or the image not {{ spin('{found on|entirely on}') }} <u>{{ $query }}</u> you can {{ spin('{contact|call}') }} us to get pictures that look for
					  {{ spin('{We provide|We offer}') }} image <strong>{{ $query }}</strong> {{ spin('{is similar|is comparable}') }}, because our website {{ spin('{focus on|concentrate on|give attention to}') }} this category, users can {{ spin('{navigate|get around|understand|find their way}') }} easily and we show {{ spin('{a simple|a straightforward}') }} theme {{ spin('{to search for|to find}') }} images that allow a {{ spin('{user|consumer|customer|end user|individual}') }} {{ spin('{to search|to find}') }}, if your pictures are on our website and want to complain, you can {{ spin('{file|document|record}') }} a {{ spin('{complaint|problem|issue|grievance}') }} by sending {{ spin('{an email|a contact}') }} is available.
				</p>

               <div class="cl"></div>
               <div class="gallery">
                  <h3>Gallery of {{ $query }}</h3>

					@foreach($results as $i => $item)
					<a rel="bookmark" href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}">
					  <img width="640" height="480" src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-full size-full" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
					</a>
					@endforeach

               </div>
               <div class="cl"></div>
            </div>

            <div class="prevnext">
               <div class="prev">
                  &laquo; <a href="{{ permalink( $random_terms[0] ) }}" rel="prev">{{ ucwords($random_terms[0]) }}</a>
               </div>
               <div class="next">
                  <a href="{{ permalink( end($random_terms) ) }}" rel="next">{{ ucwords( end($random_terms) ) }}</a> &raquo;
               </div>
               <div class="cl"></div>
            </div>

@endsection
