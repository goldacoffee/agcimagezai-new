<div id="sidebar">
<div class="searchbox right">
	<form action="{{ home_url() }}" id="searchform" method="post">
	  <input type="search" class="searchboxform" id="s" name="s" placeholder="Search here.." required />
   </form>
</div>

<div class="sidebarbox">
   <h3 class="sidebartext">Popular Post</h3>
   <div class="postlist">
   
@if($path == 'index')
	@php
		$random_terms = array_slice($random_terms, 0, 50);
	@endphp
@endif

@foreach( $random_terms as $terms ) 
  <div class="popularlink">
	  <h4><a href="{{ permalink($terms) }}" title="{{ ucwords($terms) }}" rel="bookmark">{{ ucwords($terms) }}</a></h4> 
  </div>
@endforeach

   </div>
</div>
</div>
<div class="cl"></div>
		 