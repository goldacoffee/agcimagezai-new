@extends('layout')

@section('content')

<h1 class="page_title">{{ $page_title }}</h1>
<br/>
<div class="page">
@include('page/' . strtolower($page_title) )
</div>
@endsection
