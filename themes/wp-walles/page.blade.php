@extends('layout')

@section('content')

		<div id="main" class="large-12 medium-12 small-12 columns">

<h1 class="single-title" style="color: #fff;">{{ $page_title }}</h1>

@include('page/' . strtolower($page_title) )


</div><!-- end #main-->
	




@endsection