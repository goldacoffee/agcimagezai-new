<!doctype html>
<head>
@include('header')

@include('css')

</head>

<body class="home blog left-sidebar">

	<div class="clear"></div>
	<div class="container">
    <header id = "header">
	<div class="container row">
	<div class="logo large-4 medium-4 small-12 columns">
												<div class="site-logo"><a href="/">
					<h1 style='display:none;'>{{ sitename() }}</h1>
					<img title="" src="<?php echo ""; ?>" />
					<div class="site-description">{{ sitename() }}</div>
					</a></div>
								</div><!--end logo-->
			<div class="nav-menu large-8 medium-8 small-12 columns">
<div class="menu-footer-menu-container">
<ul id="menu-footer-menu" class="top-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-84"><a href="{{ home_url() }}">Home</a></li>
		@foreach( config('themes.page') as $page )
			<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-83"><a href="{{ permalink( $page, 'page' ) }}" title="{{ $page }}">{{ ucwords($page) }}</a></li> 
		@endforeach
            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-83"><a target="_blank" href="{{ home_url('sitemap.xml') }}">Crawler</a></li>
</ul></div> 
			</div>
			</div>
		<div class="container row">
			
			<nav id ="navigation" class="large-9 medium-9 small-12 columns">
			
					<div class="nav-menu large-10 medium-8 small-12 columns">
												<div class="menu-main-menu-container">
												
</div> 
					</div>
					
					
			</nav>
			
					<div class="search-wall large-3 medium-3 small-12 columns">
						
					</div>
							
		</div>
	</header>
	</div>
	<div class="clear"></div>


<div class="container row">
	
@yield('content')

<div id="footer">
	<div class="container">
		<div class="disclaimer">			
		DISCLAIMER: All wallpapers and backgrounds found here are believed to be in the "public domain". Most of the images displayed are of unknown origin. We do not intend to infringe any legitimate intellectual right, artistic rights or copyright. If you are the rightful owner of any of the pictures/wallpapers posted here, and you do not want it to be displayed or if you require a suitable credit, then please contact us and we will immediately do whatever is needed either for the image to be removed or provide credit where it is due. All the content of this site are free of charge and therefore we do not gain any financial benefit from the downloads of any images/wallpaper.		</div>
		<div class="right">
			<div class="footer-menu container">
<div class="menu-footer-menu-container">
<ul id="menu-footer-menu-1" class="footer-menu">
<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-84"><a href="{{ home_url() }}">Home</a></li>
		@foreach( config('themes.page') as $page )
			<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-83"><a href="{{ permalink( $page, 'page' ) }}" title="{{ $page }}">{{ ucwords($page) }}</a></li> 
		@endforeach
            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-83"><a target="_blank" href="{{ home_url('sitemap.xml') }}">Crawler</a></li>
</ul>
</div> 
			</div>
		</div>
		<div class="copyright">
			Copyright &copy; <a href="/">{{ sitename() }}</a> 2017<br />
		</div>
	</div>
</div>

</div><!--end container-->

<div class="main-wrapper"></div>
@include('footer')
</body>
</html>