@extends('layout')

@section('content')

		<div id="main" class="large-12 medium-12 small-12 columns">		
		
		@include('breadcrumb')
		
				<div class="entry-attachment">
					<h1 class="single-title" style="color: #fff;">{{ $subquery }}</h1>
					
					<div class="inner-attachment">
<div class="single-ads" style="float:none;width:100%">
{!! ads('responsive') !!}
</div>
												

<a href="#" title="{{ $gallery['current']['title'] }} - {{ $query }}">
	<center><img style="width:30%" class="attachment-full size-full" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" title="{{ $gallery['current']['title'] }} - {{ $query }}" alt="{{ $gallery['current']['title'] }} - {{ $query }}"></center>
	<noscript>&lt;img src="{{ $gallery['current']['url'] }}" alt="{{ $gallery['current']['title'] }} - {{ $query }}" title="{{ $gallery['current']['title'] }} - {{ $query }}"/&gt;</noscript>
</a>
							
					</div><!-- .inner-attachment -->
					
					<div class="prev-next">
						<span class="prev full"></span>
						
						<span class="next full"></span>
					</div><div class="clear"></div>

					<div class="entry-description">
												
												
					</div><!-- .entry-description -->
					
<div class="single-ads" style="float:left;width:100%">
{!! ads('responsive') !!}
</div>
					
						
				<div class="gallery-wrap" style="width: 100%;float: left;">
					@include('spintax/search-2')
				</div>
			
						<div class=" large-12 columns">
				
	<h3 class="header-title">Related Gallery Of {{ $subquery }}</h3>
	<div class="holder">

@foreach( $results as $key => $item )
<article id="post-{{ $key }}" class="post-{{ $key }} post type-post status-publish format-standard has-post-thumbnail hentry">
		<div class="entry-content" style="position:relative">
			<a href="{{ attachment_url( $query, $item['title'] ) }}"> <img width="300" height="169" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" class="attachment-medium size-medium wp-post-image" alt="{{ $query }} - {{ $item['title'] }}" sizes="(max-width: 300px) 100vw, 300px" title="{{ $query }} - {{ $item['title'] }}"/></a>
		</div><!-- .entry-content -->
		<header class="entry-header">
			<h2 class="entry-title">
<a href="#" title="{{ $item['title'] }}" rel="bookmark">{{ $query }} - {{ $item['title'] }}</a>
			</h2>
			
		</header><!-- .entry-header -->
		<footer class="entry-meta">
			<ul><li>Size : {{ $item['size'] }}</li></ul>
			
		</footer><!-- .entry-meta -->
	</article>
@endforeach			



	</div>

	<h3 class="header-title">Related Post of {{ $query }}</h3>
	@foreach( $random_terms as $term )
	<div class="termpost">
		<a href="{{ permalink( $term ) }}" title="{{ $term }}">{{ ucwords($term) }}</a>
	</div>
@endforeach

			</div>
	</div><!-- .entry-attachment -->
</div><!-- end #main-->

@endsection