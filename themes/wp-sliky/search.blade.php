@extends('layout')

@section('content')


<h1>{{ $query }}</h1>

<div class="imagegallery">

@foreach($results as $key => $item)

<div class="ithumb">
  <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">
    <img class="class_th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
  </a>
  <p>{{ strtolower($query) }} - {{ $item['title'] }} <a target="_blank" href="{{ $item['url'] }}">.</a></p>
</div>


@endforeach

</div>

@endsection
