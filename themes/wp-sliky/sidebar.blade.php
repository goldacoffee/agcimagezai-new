<div id="sidebar">

@if( $path !== 'page' )
	{!! ads('responsive') !!}	

<p class="label">Recent Posts</p>
<ul>
@foreach ($random_terms as $index => $term)

	@if($index < 30)
		<li><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></li>
	@endif

@endforeach											
</ul>
@endif
</div>