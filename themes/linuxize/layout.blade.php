<html class="no-js dark" lang="en-us">

<head>
  <meta http-equiv="x-ua-compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
  @include('header')
  @includeIf('customcss')
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link
    href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,400;1,700&display=swap"
    rel="stylesheet">
  <!-- END EZHEAD -->
  <style type="text/css">
    * {
      font-family: monospace;
    }

    body>#ezmobfooter {
      bottom: 0px !important;
      visibility: visible;
    }

  </style>
</head>

<body class="font-sans leading-normal kind-home type-page home">
  <div class="main-wrap flex flex-col">
    <header id="header"
      class="header fixed w-full border-b border-gray-200 dark:border-gray-800 linuxize-shadow px-6 z-50">
      <nav class="w-full flex justify-start mx-auto items-center h-12 md:h-16">
        <a class="logo block flex items-center text-black dark:text-gray-200 flex-shrink-0 mr-6 no-underline" href="/">
          <span class="font-medium text-md tracking-tight ">{{ config('site.title') }}</span>
        </a>
        @foreach (range('A', 'Z') as $list)
          <div class="flex flex-1 min-w-0 text-sm -mb-px">
            <a href="{{ permalink($list, 'list_term') }}"
              class="hidden md:block no-underline text-gray-800 hover:text-gray-900 dark:text-gray-300 dark-hover:text-gray-100 mr-4"><span
                class="inline-block py-3 md:py-4 md:py-6 font-bold">{{ $list }}</span></a>
          </div>
        @endforeach







  </div>
  <div class="flex justify-end">
    <button id="btn-search" class="h-6 block" aria-label="Search">
      <svg role="img" class="fill-current text-gray-900 dark:text-gray-300 inline-block h-6 w-6" aria-hidden="true">
        <use xlink:href="/svg/sprite.svg#search"></use>
      </svg>
    </button>
    <button id="mode-toggle" class="mode-toggle ml-2 is-ready" aria-label="Disable dark mode" title="Disable dark mode">
      <span class="mode-toggle-icons">
        <svg role="img" class="fill-current text-gray-900 dark:text-gray-300 inline-block h-6 w-6" aria-hidden="true">
          <use xlink:href="/svg/sprite.svg#moon"></use>
        </svg>
        <svg role="img" class="fill-current text-gray-900 dark:text-gray-300 inline-block h-6 w-6" aria-hidden="true">
          <use xlink:href="/svg/sprite.svg#sun"></use>
        </svg>
      </span>
    </button>
  </div>
  </nav>
  </header>
  <main class="main pb-8 pt-16 sm:pt-24 w-full flex-grow flex-shrink-0">


    <div class="w-full max-w-5xl mx-auto px-6">
      <div class="post-header mb-4 xl:mb-8  relative">
        <h1 class="leading-none text-2xl lg:text-3xl xxl:text-4xl font-semibold mb-2">@yield('h1')</h1>
        <div class="flex flex-col md:flex-row text-xs font-medium text-gray-700 dark:text-gray-300">
          <div class="inline-flex flex-row">
            @yield('breadcumb')

          </div>
        </div>
      </div>

      <div class="w-full mx-auto mb-8 overflow-hidden default post-list">
        <div class="-mx-4 flex flex-wrap">


          @yield('content')


        </div>
        <div class="w-full border-t border-gray-200 dark:border-gray-800 font-mono mt-16">
          <nav class="pagination flex flex-wrap justify-center text-white-700 -mt-px">


            @foreach (range('A', 'Z') as $list)

              <a class="p-2 mx-1 border-t border-transparent hover:border-gray-700"
                href="{{ permalink($list, 'list_term') }}" rel="bookmark">{{ $list }}</a>
            @endforeach


          </nav>
        </div>
      </div>
    </div>

  </main>
  <section id="search-box" class="search bg-white absolute inset-0 w-full h-screen block z-100 hidden">
    <div class="min-h-screen flex flex-col">
      <header class="w-full flex justify-end mx-auto items-center h-12 sm:h-16 px-6 z-10">
        <a href="#" id="btn-search-close" class="h-6 sm:h-8 block">
          <svg role="img" class="fill-current text-gray-900 inline-block h-6 sm:h-8 w-6 sm:w-8" aria-hidden="true">
            <use xlink:href="/svg/sprite.svg#close"></use>
          </svg>
          <small class="w-6 sm:w-8 block text-center">ESC</small>
        </a>
      </header>
      <div class="px-6 pb-8 pt-16 sm:pt-24 w-full flex-grow flex-shrink-0">
        <div class="flex flex-col max-w-screen-xl mb-4 mb-8 mx-auto">
          <div class="searchbox-container" id="searchbox"></div>
          <div id="search-results" class="search-results"></div>
          <div id="hits"></div>
          <div id="stats"></div>
          <footer class="mt-6">
            <a href="https://github.com/algolia/instantsearch.js/" target="_blank" rel="noopener noreferrer"
              tabindex="-1">
              <svg role="img" class="fill-current text-gray-700 inline-block search-algolia-logo" aria-hidden="true">
                <use xlink:href="/svg/sprite.svg#search-by-algolia"></use>
              </svg>
            </a>
          </footer>
        </div>
      </div>
    </div>
  </section>
  <footer class="w-full bg-gray-100 dark:bg-gray-800 px-6">
    <div
      class="flex items-center justify-center lg:justify-between text-center lg:text-left flex-wrap py-8 w-full max-w-screen-xl mx-auto">
      <div class="text-sm w-full lg:w-1/2 mb-2 lg:mb-0">
        <div class="w-full block lg:flex lg:items-center lg:w-auto">
          <div class="lg:flex-grow"><span
              class="text-gray-800 dark:text-gray-300 block lg:inline-block mb-2 lg:mb-0 lg:mr-8">© 2021
              {{ config('site.url') }}</span>

            @foreach (config('themes.page') as $page)

              <a href="{{ permalink($page, 'page') }}"
                class="footer-menu-item no-underline inline-block lg:mt-0 hover:text-gray-800 text-gray-900 dark:text-gray-300 dark-hover:text-gray-100 mr-4">{{ ucwords($page) }}</a>
            @endforeach


            <a href="{{ home_url('sitemap.xml') }}"
              class="footer-menu-item no-underline inline-block lg:mt-0 hover:text-gray-800 text-gray-900 dark:text-gray-300 dark-hover:text-gray-100 mr-4">Sitemap</a>

          </div>
        </div>
      </div>

    </div>
    <span id="ezoic-pub-ad-placeholder-128" class="ezoic-adpicker-ad"></span>

  </footer>
  </div>





  <div id="uglipop_overlay_wrapper" style="position:absolute;top:0;bottom:0;left:0;right:0;display:none">
    <div id="uglipop_overlay"
      style="position: fixed; inset: 0px; opacity: 0.3; width: 100%; height: 100%; background-color: black; display: none;">
    </div>
  </div>
  <div id="uglipop_content_fixed"
    style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); opacity: 1; z-index: 10000000; display: none;">
    <div id="uglipop_popbox"></div>
  </div>
</body>

</html>
