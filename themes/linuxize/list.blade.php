@extends('layout')


@section('h1')
 Index Post With Word {{ $list_title }}
@endsection
@section('content')


  <section class="section-0 px-4 w-full mb-8">
    <ul class="randomindex">
      @foreach ($random_terms as $term)
        <section class="section-4 px-1 w-full">
          <a href="{{ permalink($term) }}"
            class="w-full py-2 flex items-center leading-normal border-b border-gray-200 dark:border-gray-800 border-t">
            <div class="flex-1">

              <h2 class="font-medium text-xl lg:text-2xl">» &nbsp;{{ ucwords($term) }}</h2>
            </div>

          </a>
        </section>

      @endforeach
    </ul>



  </section>



@endsection
