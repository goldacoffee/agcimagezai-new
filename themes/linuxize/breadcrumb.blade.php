<div class="meta" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
		<span typeof="v:Breadcrumb"><a href="{{ home_url() }}" rel="v:url" property="v:title">Home</a></span> &gt; 
		<span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ ( isset($query) ) ? $query : config('site.description') }}</a></span> &gt; 
		<span class="crent">{{ ( isset($query) ) ? $query : config('site.description') }}</span>
	</div>
</div>