@extends('layout')

@section('content')
  @php
  shuffle($random_terms);
  @endphp
  @foreach ($random_terms as $term)

    <section class="section-3 w-full md:w-1/3 px-4 mb-8">
      <a href="{{ permalink($term) }}" class="flex overflow-hidden">
        <div class="flex flex-col h-full w-full">
          <div class="overflow-hidden">

          </div>
          <div class="w-full pt-4 flex flex-col justify-between leading-normal">
            <h2 class="mb-2 font-medium text-xl">{{ $term }}</h2>
          </div>
        </div>
      </a>
    </section>
  @endforeach

@endsection
