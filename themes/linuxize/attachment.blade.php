@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('h1')
  {{ $subsuqery }}
@endsection
@section('content')


  <section class="section-0 px-4 w-full mb-8">


    <a href="{{ attachment_url($query, $gallery['current']['title']) }}" class="d-block">
      <div class="h-full md:flex flex-col">
        <div class=" bg-cover overflow-hidden">
          <figure class="relative">
            <div class="relative block w-full mx-auto my-0">
              <div class="block" style="padding-bottom:53%"></div>
              <div class="bg-transparent  absolute inset-0 w-full h-full m-auto overflow-hidden">
                <img class="absolute inset-0 h-full m-auto ezlazyloaded" src="{{ $gallery['current']['url'] }}"
                  onerror="this.onerror=null;this.src='{{ $gallery['current']['url'] }}';" alt="{{ $gallery['current']['title'] }}"
                  title="{{ $gallery['current']['title'] }}">
              </div>
            </div>
          </figure>
        </div>

      </div>
    </a>
  </section>
  <section class="px-4 w-full mb-8">

    <div class="h-full">
      <table class="table table-striped table-auto w-full" style="font-size: 12px;">
        <thead>
          <tr>

            <th>Image atribute</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <tr>

            <td>Title</td>
            <td>: {{ $subquery }}</td>
          </tr>
          <tr>

            <td>Upload by</td>
            <td>: Admin</td>
          </tr>
          <tr>

            <td>Upload date</td>
            <td>: {{ date('d/m/y') }}</td>
          </tr>
          <tr>

            <td>Image link</td>
            <td>: <a href="{{ $gallery['current']['url'] }}" title="{{ $gallery['current']['title'] }}"
                target="_blank">Large</a>, <a href="{{ $gallery['current']['thumbnail'] }}"
                title="{{ $gallery['current']['title'] }}" target="_blank">Small</a>
            </td>
          </tr>
          <tr>

            <td>Size</td>
            <td>: {{ $gallery['current']['width'] }} x {{ $gallery['current']['height'] }}</td>
          </tr>
          <tr>

            <td>Source</td>
            <td>: {{ $gallery['current']['domain'] }}</td>
          </tr>
        </tbody>
      </table>
    </div>

  </section>

  @foreach ($results as $i => $item)
    @if ($i !== 0)
      <section class="section-1 w-full md:w-1/3 px-4 mb-8">
        <a href="{{ attachment_url($query, $item['title']) }}" class="flex overflow-hidden"
          title="{{ $item['title'] }}" rel="bookmark" itemprop="associatedMedia" itemscope=""
          itemtype="http://schema.org/ImageObject">
          <div class="flex flex-col h-full w-full">
            <div class="overflow-hidden">
              <figure class="relative">
                <div class="relative block w-full mx-auto my-0">
                  <div class="block" style="padding-bottom:53%"></div>
                  <div class="bg-gray-100 absolute inset-0 w-full h-full m-auto overflow-hidden">
                    <img class="absolute inset-0 w-full h-full m-auto ezlazyloaded"
                      src="{{ filterImage($item['url']) }}"
                      onerror="this.onerror=null;this.src='{{ $item['thumbnail'] }}';" alt="{{ $item['title'] }}"
                      title="{{ $item['title'] }}">
                  </div>
                </div>
              </figure>
            </div>
            <div class="w-full pt-4 flex flex-col justify-between leading-normal">
              <h2 class="mb-2 font-medium text-md sm:text-xl">{{ $item['title'] }}</h2>
              {{-- <p class="text-base">
             
            </p> --}}
            </div>
          </div>
        </a>
      </section>

      @if ($i % 6 == 0)
        @php
          shuffle($random_terms);
        @endphp
        <div class="px-4 m-auto mb-4 w-full flex flex-wrap">
          @foreach ($random_terms as $keyRel => $term)
            @if ($keyRel < 3)
              <section class="section-1 w-full md:w-1/3 mb-1 px-1">
                <span
                  class="inline-block w-full  bg-gray-50  px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#{{ $term }}</span>
              </section>
            @endif
          @endforeach
        </div>

      @endif
    @endif


  @endforeach







  {{-- @include('breadcrumb') --}}

  {{-- <div id="adr_contentfront">
   <div id="leftxhouse">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
         <h1 itemprop="headline" class="headxhouse">{{ $query }}</h1>
		 @yield('ads')
         <div class="xhouseimg" itemprop="articleBody">
					  <img width="1187" height="780" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" class="attachment-full size-full" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}">
         </div>
		 @yield('ads')
		 <div class="rads">
			 <div class="nextat"><a href="{{ attachment_url( $query, $gallery['next']['title'] ) }}">Next Image</a></div>

			 <div class="prevat"><a href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}">Previous Image</a></div>
		 </div>

         <div class="cl"></div>
         <div class="xhousecontent">
            <div itemprop="articleBody">

<div class="xhousecontent">
   <h2>{{ $subquery }}</h2>
   <br>

   <p>Above photo is <strong>{{ $subquery }}</strong> posted by <i><a href="{{ home_url() }}" title="{{ sitename() }}">{{ sitename() }}</a></i> on {{ date('d/m/y') }}. The picture was taken and seen by 3 User and has been downloaded and reviewed {{ rand(100,1000) }} Times. You can use the image as background for your computer desktop and laptop screen, because this image has a size of has {{ $gallery['current']['size'] }} Pixel. If you want to save to a personal computer, you can download this image in full size. Please click "Download picture in full size" at the end of the article, then, your device automatically saves these images into a directory on your disk space. If you want to download more images from our collection, please click on the image below and do the same steps, the full size download.</p>
   <br>
   <p>Change the picture in the color display with <strong>{{ $subquery }}</strong> is the right thing to get rid of your {{ $query }} on desktop computers, pc, computer, laptop, you use every day to work or just plain, perform daily activities. An image has an aura, which penetrates the feeling of someone, for example images, sees that motivation by you evoke the image then the image that there was joy, and even images, feelings of sadness to create.</p>
   <br>
   <p>Well we have collected this time some ideas work, can change the atmosphere of your Office or computer screen. You can several downloaded and stored in the computer's memory, or you can download all photos from our website free of charge. If you {{ $query }} to download, you should now work in full-screen mode, see this article, until the end of.</p>
   <br>
   <p></p>
   <p><strong>Detail pictures for {{ $subquery }}</strong></p>
   <br>
   <table class="table table-striped" style="font-size: 12px;">
      <thead>
         <tr>
            <th>No</th>
            <th>Image atribute</th>
            <th>Value</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td>1</td>
            <td>Title</td>
            <td>:{{ $subquery }}</td>
         </tr>
         <tr>
            <td>2</td>
            <td>Upload by</td>
            <td>:Admin</td>
         </tr>
         <tr>
            <td>3</td>
            <td>Upload date</td>
            <td>:{{ date('d/m/y') }}</td>
         </tr>
         <tr>
            <td>4</td>
            <td>Image link</td>
            <td>:<a href="{{ $gallery['current']['url'] }}" title="{{ $gallery['current']['title'] }}">Large</a>, <a href="{{ $gallery['current']['small'] }}" title="{{ $gallery['current']['title'] }}">Small</a></td>
         </tr>
         <tr>
            <td>5</td>
            <td>Size</td>
            <td>:{{ $gallery['current']['size'] }}</td>
         </tr>
         <tr>
            <td>6</td>
            <td>Copyright</td>
            <td>:{{ $gallery['current']['copy'] }}</td>
         </tr>
      </tbody>
   </table>
   <p></p>
</div>


            </div>

            <div class="cl"></div>
         </div>

		<div class="lbox">
		<div class="xhousecl">
		   <h3 class="xhouse_up" itemprop="headline">Gallery of {{ $query }}</h3>
				@foreach ($results as $i => $item)
				<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}" rel="bookmark" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject" >
				  <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
				</a>
				@endforeach
		</div>
		<div class="cl"></div>
		</div>
      </article>
   </div>
   <aside id="xhouseside" role="complementary" itemscope="" itemtype="http://schema.org/WPSidebar">
      <div class="sidebarbox">
         <div class="ads_sidebar">
            <center>@yield('ads')</center>
         </div>
         <h3 class="xhouse_up">Popular Post</h3>
         <div class="postlist">

			 @foreach ($random_terms as $term)
				<div class="popularlink">
					<h4><a title="{{ ucwords($term) }}" href="{{ permalink($term) }}" rel="bookmark">{{ ucwords($term) }}</a></h4>
				</div>
			 @endforeach

         </div>
      </div>
   </aside>
   <div class="cl"></div>
</div> --}}

@endsection
