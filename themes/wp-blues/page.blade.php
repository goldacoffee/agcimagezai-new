@extends('layout')

@section('content')

<h1>{{ $page_title }}</h1>

@include('page/' . strtolower($page_title) )

@endsection