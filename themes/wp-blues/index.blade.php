@extends('layout')

@section('content')

  <h1>Welcome : {{ sitename() }} - {{ config('site.description') }}</h1>

  <ul class="c listhome">
    @foreach ($random_terms as $term)
      <li><a href="{{ permalink($term) }}" title="{{ ucwords($term) }}">{{ ucwords($term) }}</a></li>
    @endforeach
  </ul>

@endsection
