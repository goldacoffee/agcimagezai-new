<style>

/*
Theme Name: master
Description: theme wordpress.
Version: 2.0
Author: G
*/
*{margin:0;padding:0;}
body{font-size:12px;color:#B8B7B5;font-family:arial, verdana, georgia;background:rgba(10, 10, 10, 0.91);}
.left{float:left;}
.right{float:right;}
.clear{clear:both;}
a{color:#7FFF00;text-decoration:none;}
a:hover{text-decoration:none;color:#fff;}
strong a{text-decoration:none;color:#7FFF00;}
strong a:hover{text-decoration:none;color:#7FFF00;}
h1,p{margin-top:10px;}
h2{margin:5px 0;}
h4{margin:5px 0 0 5px;font-size:20px;color:#7FFF00;}
h5,h3{margin:5px 0 0 5px;font-size:10px;color:#B8B7B5;text-transform: uppercase;}
#master_container{width:1004px;margin:0 auto; margin-top:70px; background:#0a0a0a;padding:10px 10px;}
#master_head{margin: 0 auto;width: 100%;background: rgba(32, 32, 32, 0.91);position:absolute;top: 0;z-index: 10;padding-bottom: 5px;}
.master_header{text-align: left;width: 1004px;background: #1f1f1f;margin: 0 auto;padding-left: 10px;}
.master_header h1{font-size:12px;}
.master_header h2{font-size:12px;margin:0;margin-bottom: 15px;}
.master_header a{font-size:30px;font-family:arial;color:#B8B7B5;padding:0px 10px 0px 0px;text-decoration:none;font-weight:bold;text-shadow:0 1px 2px #0a0a0a;}
.master_notice{font-size:13px;}
.master_notice a{color:#7FFF00;font-size:13px;}
#master_content{width:786px;float:left;padding:5px 0;border-right: 1px dashed rgba(10, 10, 10, 0.91);}
.master_posting{width:241px;background:#0a0a0a;float:left;margin:8px 0 0 18px;position:relative;font-family:arial;border-top: 1px solid #7FFF00;}
.master_posting h2{text-align:center;font-size:11px;margin-top:8px;}
.master_posting h3{text-align:center;margin-top:-3px;color:#fff;font-size:13px;font-weight:normal;}
.master_posting h3 a{color:#a0a0a0;}
.master_posting a{text-decoration:none;color:#B8B7B5;text-transform:uppercase;}
.master_posting a:hover{text-decoration:none;color:#7FFF00;}
.master_posting p a{text-decoration:none;color:#a0a0a0;}
.master_postinginfo{padding:15px;font-size: 13px;}
.ttlmaster_posting{width: 231px;height: 30px;padding: 5px;text-align: center;text-transform: uppercase;position: absolute;margin-top: -40px;background: rgba(10, 10, 10, 0.91);}
.attachment-featured:hover{opacity: 0.4;filter: alpha(opacity=40);}
.attachment-thumb:hover{opacity: 0.4;filter: alpha(opacity=40);}
.master_readmore{float:right;}
.master_single{width:780px;float:left;padding:5px 0;}
.master_single h1{padding:7px 10px;color:#B8B7B5;font-size:16px;font-family:'verdana';font-weight:bold;background-color:#0A0A0A;text-transform:uppercase;}
.master_single h2 {margin:10px;font-size: 13px; font-family: arial;text-transform: uppercase;}
.master_single p{padding:0px 10px;text-align:justify;font-size:13px;line-height: 25px;}
.master_page{width:900px;font-size: 14px;float:left;margin:10px;line-height: 25px;}
.master_page p{margin:10px;}
.master_page li{margin-left:30px;}
.master_title h3{color: #B8B7B5;margin-left: 20px;font-size: 13px;text-transform:uppercase;}
.master_attachment{width:786px;float:left;border-right: 1px dashed rgba(10, 10, 10, 0.91);}
.master_attachment h1{padding: 7px 10px;color: #B8B7B5;font-size: 16px;margin-top: 2px;font-family: 'verdana';font-weight: bold;background-color: ##1f1f1f;text-transform: uppercase;}
.master_attachment h2 {margin:10px;font-size: 11px; font-family: arial;}
.master_attachment p{padding:0px 10px;text-align:justify;font-size:13px;line-height: 25px;}
.attachment-huge{width:786px; height:auto;}
.attachment-large{width:780px;height:auto;padding-bottom: 5px;}
.attachment-large:hover{opacity: 0.4;filter: alpha(opacity=40);}
.attachment-thumbnail{width:110px;max-height:110px;height:120px;margin-right:10px;margin-bottom:10px;}
.attachment-featured{width:675px;height:300px;margin-bottom: 5px;margin-top:10px;border-radius:10px;}
.attachment-thumb{width: 164px;height: 78px;border: 3px solid #1F1F1F;margin-left: 12px;padding: 5px;margin-top: 10px;}
.attachment-featured-home{width:241px;height:150px;margin-top:5px;}
.attachment-featured-thumb{float:left;width: 90px;}
.breadat{width:781px;float:left;color:#a0a0a0;font-size:12px;font-family:arial;padding: 5px 0px 4px 0;border-bottom: 1px dashed #7FFF00;}
.breadat a{color:#7FFF00text-decoration:none;}
.breadat a:hover{color:#7FFF00;text-decoration:none;}
.breadsg{width: 781px;float: left;padding-bottom:4px;color: #a0a0a0;font-size: 12px;margin-bottom: -12px;font-family: arial;background: #1f1f1f;}
.breadsg a{color:#7FFF00;text-decoration:none;}
.breadsg a:hover{color:#7FFF00;text-decoration:none;}
#master_sidebar{width:200px;margin-top:-10px;float:right;padding:8px;font-family:arial;}
.master_sidebarname{background: #1F1F1F;padding: 8px 0px 7px 9px;font-size: 12px;text-transform: uppercase;font-family: arial;color: #7FFF00;width: 97%;margin-bottom: 10px;}
.master_sidebarbox a{color:#7FFF00;text-decoration:none;font-size:12px;font-weight:bold;}
.master_sidebarbox a:hover{text-decoration:none;color:#7FFF00;font-weight:bold;}
.master_sidebarbox ul { list-style-type: none; margin-left: 0px; width:230px;}
.master_sidebarbox ul a { color: #B8B7B5; font-size: 13px; text-transform: uppercase;}
.master_sidebarbox ul a:hover { color: #7FFF00; text-decoration: none;}
.master_sidebarbox ul li { margin-bottom: 1px; margin-right:5px; display: inline-block;padding: 10px;width: 277px;}
.master_sidebarinfo {float: left;width:100%;margin-left: 5px;}
.master_sidebarpop {width: 105%;height: 20px;padding: 5px;border-bottom: 1px solid #252525;border-right: 1px solid #252525;border-left: 1px solid #252525;border-top: 1px solid #4b4b4b;}
.master_sidebarpopular{width: 91%;}
.master_search{width: 91%; background: #1f1f1f; margin-top:5px; padding: 7px 10px; border:1px solid #1f1f1f; letter-spacing: 1px; float: left; font-family: verdana; font-weight: bold; font-size: 12px; color: #B8B7B5;}
.page_navigation{width:auto;text-align:center;margin-bottom:20px;margin-top:30px;clear:both;}
.page_navigation a, .wp-pagenavi span{text-decoration:none;font-weight:bold;border:2px groove #555;padding:3px 5px;margin:2px;color:#a0a0a0;}
.page_navigation a:hover, .wp-pagenavi span.current{color:#a0a0a0;border:2px groove #7FFF00;padding:3px 5px;}
.page_navigation span.current{font-weight:bold;border:2px groove #7FFF00;padding:3px 5px;}
#master_content nav.nav-single-top{margin:10px 0 25px 0;}
#master_content nav{padding:10px;margin:22px 0 0 0;max-width:100%;height:auto;overflow:hidden;font-weight:bold;background:#1f1f1f;}
.nav-previous1{max-width:50%;float:left;}
.nav-next1{max-width:50%;float:right;}
.nav-previous1 a, .nav-next a{text-decoration:none;color:#7FFF00;}
.nav-previous1 a:hover, .nav-next a:hover{text-decoration:none;color:#999;}
.secondary-navigation a .sf-sub-indicator{font-size:10px;color:#ADADAD;position:absolute;right:12px;top:22px;}
.secondary-navigation ul > ul a .sf-sub-indicator{top:18px;right:-10px;}
.master_attachment nav.nav-single-top{margin:0 0 10px 0;}
.master_attachment nav{padding:10px;margin:22px 0 0 0;max-width:100%;height:auto;overflow:hidden;font-weight:bold;font-size: 16px;background:#1F1F1F;}
.nav-previous{max-width:50%;float:left;}
.nav-next{max-width:50%;float:right;}
.nav-previous a, .nav-next a{text-decoration:none;color:#fff;}
.nav-previous a:hover, .nav-next a:hover{text-decoration:none;color:#999;}
.secondary-navigation a .sf-sub-indicator{font-size:10px;color:#ADADAD;position:absolute;right:12px;top:22px;}
.secondary-navigation ul > ul a .sf-sub-indicator{top:18px;right:-10px;}
#master_footer{font-size: 15px;}
.master_footer a{text-align:justify;text-decoration:none;}
.master_footer a:hover{text-decoration:none;}
.master_footers{}
.master_sosmed{padding:0px;}
.master_sosmed a{display:inline-block;margin:0px 0px 5px 0;padding:5px 8px;color:#fff;font-weight:bold;text-align:center;text-decoration:none;border-radius:5px;-moz-border-radius:5px;-o-border-radius:5px;-webkit-border-radius:5px;-ms-border-radius:5px;}
.twitter{background:#12A6CE}
.facebook{background:#3B5998;}
.pinterest{background:#A02211;}
.google{background:#D34836;}
.master_top:hover {color: #fff;background-color: #7FFF00;text-decoration: none;}
.master_top {position: fixed;bottom: 1rem;right: 1rem;width: 3.2rem;height: 3.2rem;line-height: 3.2rem;font-size: 2.4rem;color: #fff;background-color: rgba(84, 82, 82, 0.68);text-decoration: none;border-radius: 3.2rem;text-align: center;cursor: pointer;}
@media screen and (max-width:1000px){#master_container{width:728px;}
#master_content{width:98%;}
.master_header{width:728px;}
.master_posting{width:49%;margin-left: 5px;}
.breadat{width:100%;margin-top: 10px;}
.breadsg{width:100%;margin-top: 10px;}
.ttlmaster_posting{width:96%;}
.master_attachment{width:100%;}
.master_page{width:100%}
.master_single{width:100%}
img.attachment-featured-thumb{width:14%;height:80%;}
img.attachment-featured-home{width:93%;height:155px;padding: 10px;}
img.attachment-huge, img.attachment-large{width:100%;height:40%;}
img.attachment-thumbnail{width:15%;height:70%;}
#master_sidebar{width:94%;}
.master_sidebarpop {width:109%}
.master_sidebarbox{width:95%;}
.master_sidebarname{width:100%}
.master_sidebarbox ul li {width:113%}
.master_sidebarinfo{width:80%;}
.master_sidebarinfo h3{font-size:13px;}
.master_search{width:94%;}
}
@media screen and (max-width:727px){#master_container{width:520px;}
#master_content{width:98%;}
.master_header{width:520px;}
.master_posting{width:49%;margin-left: 5px;}
.breadat{width:100%;margin-top: 10px;}
.breadsg{width:100%;margin-top: 10px;}
.ttlmaster_posting{width:96%;}
.master_attachment{width:100%;}
.master_page{width:100%}
.master_single{width:100%}
img.attachment-featured-thumb{width:19%;height:80%;}
img.attachment-featured-home{width:93%;height:155px;padding: 10px;}
img.attachment-huge, img.attachment-large{width:100%;height:40%;}
img.attachment-thumbnail{width:20%;height:70%;}
#master_sidebar{width:94%;}
.master_sidebarpop {width:109%}
.master_sidebarbox{width:95%;}
.master_sidebarname{width:100%}
.master_sidebarbox ul li {width:113%}
.master_sidebarinfo{width:65%;}
.master_sidebarinfo h3{font-size:13px;}
.master_search{width:94%;}
}
@media screen and (max-width:519px){#master_container{width:300px;}
#master_content{width:98%;}
.master_header{width:300px;}
.master_posting{width: 99%;margin-left: 0px;}
.breadat{width:100%;margin-top: 23px;}
.breadsg{width:100%;margin-top: 23px;}
.ttlmaster_posting{width:96%;}
.master_attachment{width:100%;}
.master_page{width:100%}
.master_single{width:100%}
img.attachment-featured-thumb{width:34%;height:80%;}
img.attachment-featured-home{width:93%;height:155px;padding: 10px;}
img.attachment-huge, img.attachment-large{width:100%;height:40%;}
img.attachment-thumbnail{width:29%;height:70%;}
#master_sidebar{width:94%;}
.master_sidebarpop {width:109%}
.master_sidebarbox{width:95%;}
.master_sidebarname{width:100%}
.master_sidebarbox ul li {width:113%}
.master_sidebarinfo{width:64%;}
.master_sidebarinfo h3{font-size:10px;}
.master_search{width:94%;}
}

</style>
