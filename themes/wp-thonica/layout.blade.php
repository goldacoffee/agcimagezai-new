<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en-US" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:og="//opengraphprotocol.org/schema/" xmlns:fb="//www.facebook.com/2008/fbml">

<head>
 	@include('header')
  @include('css')
  <style type="text/css">
		.recentcomments a{
			display:inline !important;padding:0 !important;margin:0 !important;
			}
	</style>
</head>

   <body itemscope itemtype="http://schema.org/Webpage">
      <div id="master_head">
         <div class="master_header">

            <h1><a href="{{ home_url() }}" title="{{ config('site.title') }}">{{ sitename() }}</a></h1>

            <div class="master_notice">
							<i>All of the pictures on this website was taken from source that we believe as "Public Domain", If you want to claim your image please <a href="{{ permalink('contact', 'page') }}" rel="nofollow" title="Contact Us">Contact Us</a></i>
						</div>
            <div class="clear"></div>
         </div>
      </div>
      <div id="master_container">


        @yield('content')


				@if( !is_page() )
        	@include('sidebar')
				@endif

         <div class="clear"></div>
      </div>

      <div id="master_footer">
         <div class="master_footers">
            <center>
               <p>
								 @foreach( config('themes.page') as $page )
									 <a href="{{ permalink($page, 'page') }}" rel="nofollow" title="{{ $page }}">{{ $page }}</a> |
								 @endforeach
								 	 <a href="{{ home_url('sitemap.xml') }}" title="Crawl">Sitemap</a>
								 	 <a href="#" class="master_top">↑</a>
							 </p>
               <p>&copy; {{ date('Y') }} {{ sitename() }} &#9829; copyright are belong to the creators &#9829;</p>
            </center>
         </div>
      </div>

      @include('footer')

   </body>
</html>
