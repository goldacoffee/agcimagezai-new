<div id="s" class="kanan">
  <div class="mb15 ads160">
    {{  ads('responsive') }}
  </div>
  <p class="st">Most View</p>
  <ul>
    @foreach( array_slice($random_terms, 0, 25) as $term)
      <li>
        <a rel='bookmark' href='{{ permalink($term) }}' title='{{ $term }}'>{{ ucwords( $term ) }}</a>
      </li>
    @endforeach
  </ul>
</div>
<div class="c"></div>
