@extends('layout')

@section('content')

<div id="c">
<h1>{{ $query }}</h1>

@include('breadcrumb')

  @for( $i=0; $i<=5; $i++ )
        <div class="ms c">
          <a rel="bookmark" href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}">

            <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}" width="728" height="612"/>

          <noscript>&lt;img src="{{ $results[$i]['url'] }}" title="{{ $results[$i]['title'] }}" alt="{{ $results[$i]['title'] }}" width="990" height="832"/&gt;&lt;img src="{{ $results[$i]['url'] }}" title="{{ $results[$i]['title'] }}" alt="{{ $results[$i]['title'] }}" width="933" height="784"/&gt;&lt;img src="{{ $results[$i]['url'] }}" title="{{ $results[$i]['title'] }}" alt="{{ $results[$i]['title'] }}" width="103" height="70"/&gt;</noscript>
        </a>
        <h2 class="ts">{{ ucwords($results[$i]['title']) }}</h2></div>
  @endfor

  <blockquote>
  <p>
    {{ random_strings($results, 10, '. ', 'ucwords') }}
  </p>
  </blockquote>

<div class="g gs">
  @foreach( $results as $item )
	<a rel="bookmark" href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}">
          <img src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" width="103" height="70"/>
          <noscript>
            <img src="{{ $item['url'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" />
          </noscript>
        </a>
  @endforeach
  <div class="c"></div>
</div>


  <div class="c"></div>
  <div class="ac jn">
    @foreach( range(1,30) as $pagin )
            <a href="{{ home_url() }}?page={{ $pagin }}" title="Page {{ $pagin }}">&#8226;</a>
    @endforeach
  </div>
</div>

@include('sidebar')

@endsection
