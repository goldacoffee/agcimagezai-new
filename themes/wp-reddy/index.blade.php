@extends('layout')

@section('content')

<div id="c">
<h1>{{ config('site.title') }}</h1>

@include('breadcrumb')

  @foreach( $results as $i => $item )
      <div class="mi">
        <a href="{{ attachment_url( $query, $item['title'] ) }}" rel="bookmark" title="{{ $item['title'] }}">
          <img src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" width="728" height="546"/>
          <noscript>
            <img src="{{ $item['url'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" width="728" height="546" />
          </noscript>
		  </a>
        <h2 class="jh c">
          <a href="#" title="{{ $item['title'] }}">&#160;</a>{{ ucwords( $item['title'] ) }}
        </h2>
      </div>
  @endforeach

  <div class="c"></div>
  <div class="ac jn">
    @foreach( range(1,40) as $pagin )
            <a href="{{ home_url() }}?page={{ $pagin }}" title="Page {{ $pagin }}">&#8226;</a>
    @endforeach
  </div>
</div>

@include('sidebar')

@endsection
