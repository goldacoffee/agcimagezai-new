@extends('layout')

@section('content')

<div id="c">
<h1>{{ $page_title }}</h1>

@include('breadcrumb')

@include('page/' . strtolower($page_title) )

  <div class="c"></div>
</div>

@include('sidebar')

@endsection
