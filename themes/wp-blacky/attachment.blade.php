@extends('layout')

@section('content')

<h1 class="class_ld">{{ $query }} - {{ $gallery['current']['title'] }}</h1>

<div class="ads-top">
{!! ads('responsive') !!}
</div>

<figure>

	<img src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';"  class="attachment-full wallpaper" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}"/>

	<figcaption>{{ $subquery }}</figcaption>
</figure>


<div class="ads-top">
{!! ads('responsive') !!}
</div>

@foreach($results as $key => $item)

<div class="class_box">
  <a href="{{ attachment_url( $query, $item['title'] ) }}" class="th" title="{{ strtolower($query) }} {{ $item['title'] }}">
    <img class="th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
  </a>
  <h2>{{ strtolower($query) }} - {{ $item['title'] }}</h2>
</div>

@endforeach

@endsection
