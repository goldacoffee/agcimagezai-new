@extends('layout')

@section('title')
{{ $page_title }}
@endsection

@section('content')
<h1 class="ld">{{ $page_title }}</h1>

<br/>
<div class="pages">
@include('page/' . strtolower($page_title) )
</div>
@endsection
