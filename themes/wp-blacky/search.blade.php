@extends('layout')

@section('title')
{{ sitename() }} - {{ config('site.description') }}
@endsection

@section('content')

<h1 class="class_ld">{{ $query }}</h1>

<div class="class_ads-top">
{!! ads('responsive') !!}
</div>

@foreach($results as $key => $item)

<div class="class_box">
  <a href="{{ attachment_url( $query, $item['title'] ) }}" class="th" title="{{ strtolower($query) }} {{ $item['title'] }}">
    <img class="class_th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
  </a>
  <h2>{{ strtolower($query) }} - {{ $item['title'] }}</h2>
</div>


@endforeach

@endsection
