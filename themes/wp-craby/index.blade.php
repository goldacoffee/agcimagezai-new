@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('sidebar')
  @include('sidebar')
@endsection

@section('content')

      <h1 class="usb3">Welcome to {{ sitename() }}</h1>
      <div class="Iklan">
        @yield('ads')
      </div>

      <div id="usb2">

      @php
			$cols   = array_group( $results, 5 );
      @endphp

      @foreach ($cols as $item)
            <div class="raw">
            <div class="thumb">
                  @for( $i=0; $i<=count($item)-2; $i++ )
                    <span class="telorinsi">
                      <a href="{{ attachment_url( $query, $item[$i]['title'] ) }}" rel="bookmark" title="{{ $item[$i]['title'] }}">
                        <img width="90" height="60" src="{{ $item[$i]['small'] }}" data-src="{{ ucwords( $item[$i]['url'] ) }}" onerror="this.onerror=null;this.src='{{ $item[$i]['small'] }}';"  class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $item[$i]['title'] }}" title="{{ $item[$i]['title'] }}"/>
                      </a>
                    </span>
                  @endfor
            </div>
      @php
        $lastitem = end($item);
      @endphp

            <div class="war">
                <a href="{{ attachment_url( $query, $lastitem['title'] ) }}" rel="bookmark" title="{{ ucwords( $lastitem['title'] ) }}">
                  <img style="width:230px;max-height:130px" src="{{ $lastitem['small'] }}" data-src="{{ $lastitem['url'] }}" onerror="this.onerror=null;this.src='{{ $lastitem['small'] }}';" class="attachment-featured-home size-featured-home" alt="{{ ucwords( $lastitem['title'] ) }}" title="{{ ucwords( $lastitem['title'] ) }}" />
                </a>
                <div class="byone">
                    <a href="#" rel="bookmark" title="{{ ucwords( $lastitem['title'] ) }}"><h2>{{ ucwords( $lastitem['title'] ) }}</h2></a>
                </div>
            </div>

            <div class="posted">Posted : June 8, 2017 at 8:27 am</div>
            </div>
      @endforeach

      <div class="cl"></div>
      <div class="Iklan">

      @yield('ads')

      </div>
      <div class="cl"></div>

@php
  $pagination = range(1,20)
@endphp

      <div id="soup">
       <div class="wp-pagenavi">
         <span class="pages">Page 1 of {{ end($pagination) }}:</span>

         @foreach($pagination as $n => $page)
            @if( input()->get('page') == $n+1 )
                <strong class='current'>{{ $page }}</strong>
            @elseif( $page == end($pagination) )
                <a href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
            @else
                <a href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
            @endif
         @endforeach

       </div>
         <div class="clearfix"></div>
      </div>

      </div>

      @yield('sidebar')

@endsection
