@extends('layout')

@section('content')
<div id="usb2a">

<div class="Iklan">
{!! ads('responsive') !!}
</div>

<div class="loo">
<h1 class="laa">{{ $subquery }} - {{ $query }}</h1>

<div class="lee">
<img width="1409" height="1000" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';"  class="attachment-full size-full" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}"/>
<h2><strong>{{ $subquery }} - {{ $query }}</strong></h2>
</div>

<div class="lii">

<a href="//www.pinterest.com/pin/create/button/?url={{ urlencode( get_permalink() ) }}&amp;media={{ $gallery['current']['url'] }}&amp;description={{ $gallery['current']['title'] }}" data-pin-do="buttonPin" data-pin-config="beside">
<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png"></a>

<div class="e">
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
<g:plusone></g:plusone>
</div>
<div class="luu">
<iframe src="http://www.facebook.com/plugins/like.php?href={{ urlencode( get_permalink() ) }}&amp;layout=button_count&amp;show_faces=false&amp;
width=60&amp;action=like&amp;colorscheme=dark" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; height: 30px; width:80px; "></iframe>
</div>
</div>

<div class="Iklan">
{!! ads('responsive') !!}
</div>

<div class="jump">
<div class="youa">
<a href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}">Prev Image</a>
</div>
<div class="tooa">
<a href="{{ attachment_url( $query, $gallery['next']['title'] ) }}">Next Image</a>
</div>
<div class="cl"></div>
</div>

<blockquote>
  <strong>{{ $subquery }} - {{ $query }}</strong> part of <a href="{{ get_permalink() }}" rev="attachment">{{ $query }}</a>. {{ $query }}  is one {{ spin('{of our|in our|of our own|of the|of your}') }} collection.<br>
We choose the image option for display
We {{ spin('{paid attention to|taken notice of}') }} you to {{ spin('{provide a|give a}') }} good picture and with {{ spin('{high definition|hi-def}') }} (HD). {{ spin('{If you want to|If you wish to}') }} keep this image right click {{ spin('{directly on|on}') }} the picture. {{ spin('{Select|Choose}') }} "Save As.." and than choose {{ spin('{the location|the positioning}') }} or the folder where {{ spin('{will you|do you want to}') }} keep this picture, .Jpg is a default format picture, you can also change the format picture, {{ spin('{the way|just how}') }} is at {{ spin('{will save|helps you to save}') }} in {{ spin('{storage|storage space|safe-keeping|storage area}') }} the image {{ spin('{can add|can truly add}') }} {{ spin('{extension|expansion}') }} or add other {{ spin('{extension|expansion}') }} as .png .jpeg, then {{ spin('{you will get|you can get}') }} image {{ $subquery }} with the {{ spin('{resolution|quality|image resolution}') }} are {{ spin('{equal to|add up to}') }} in {{ spin('{your computer|your personal computer}') }}.<br><br>
{{ spin('{You can see|You can view}') }} the picture in a gallery other similar with {{ spin('{previous|earlier|prior|past}') }} navigate using the image and {{ spin('{the next|another}') }} image that will {{ spin('{facilitate|help|assist in|aid|help in|accomplish}') }} you in venturing website. The picture {{ spin('{you see|you observe|the thing is|the truth is|the thing is that|the simple truth is}') }} in {{ spin('{the public|the general public}') }} domain and {{ spin('{in our|inside our}') }} website {{ spin('{will be the|would be the}') }} same, {{ spin('{so you|and that means you|which means you}') }} will {{ spin('{have no|have not any}') }} trouble finding pictures is.<br>
This image is one of {{ spin('{the existing|the prevailing}') }} images in the <strong>{{ $subquery }}</strong>, {{ spin('{we provide|we offer}') }} other similar images {{ spin('{in one|in a single}') }} post, {{ spin('{so this|which means this}') }} {{ spin('{will allow|allows}') }} a user {{ spin('{in search of|searching for}') }} a {{ spin('{wanted|desired|needed|wished|required|sought}') }} picture.

</blockquote>

<div class="sugiarto">
<h3>{{ $query }} Advertisement</h3>
{!! ads('responsive') !!}
</div>

<div class="laga">
<h3>Gallery of {{ $subquery }} - {{ $query }}</h3>

@foreach($results as $i => $item)
<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" rel="bookmark" title="{{ $results[$i]['title'] }}">
 <img width="90" height="60" src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
</a>
@endforeach

</div>
<div class="cl"></div>

</div>

	<div class="cl"></div>
</div>
<div class="cl"></div>
@endsection
