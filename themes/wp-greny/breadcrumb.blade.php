<div class="breadchumb" xmlns:v="http://rdf.data-vocabulary.org/#">
	<span typeof="v:Breadcrumb"><a href="{{ home_url() }}" rel="v:url" property="v:title">Home</a></span> »
	@if( is_home() )
			<span class="current1" typeof="v:Breadcrumb">{{ config('site.description') }}</span>
	@elseif( is_page() )
		<span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $page_title }}</a></span> »
		<span class="current1"  typeof="v:Breadcrumb">{{ $page_title }}</span>
	@elseif( is_search() )
		<span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $query }}</a></span> »
		<span class="current1" typeof="v:Breadcrumb">{{ $query }}</span>
	@elseif( is_attachment() )
		<span typeof="v:Breadcrumb"><a href="{{ permalink($query) }}" rel="v:url" property="v:title">{{ $query }}</a></span> »
		<span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $subquery }}</a></span> »
		<span class="current1" typeof="v:Breadcrumb">{{ $subquery }}</span>
	@endif
</div>
