@extends('layout')

@section('content')


@include('breadcrumb')


      <h1 class="lead">{{ $query }}</h1>
      <div id="content_left">
        <div class="ikland">{!! ads('responsive') !!}</div>
        <div class="single_content">
          <div class="bigimages" style="float:center">

            <div class="singlet">
				<span class="adagambar">
					<a href="{{ attachment_url( $query, $results[0]['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords($results[0]['title']) }}">
						<img class="attachment-large size-large" data-src="{{ $results[0]['url'] }}" src="{{ $results[0]['small'] }}" alt="{{ ucwords($results[0]['title']) }}" title="{{ ucwords($results[0]['title']) }}"></a>
				</span>
              	<p>{{ ucwords($results[0]['title']) }}</p>

			</div>

			<div class="iklan">{!! ads('responsive') !!}</div>

			<div class="postsinglegal">
              <div class="postgallery">
                <h2 class="gallerytitle">Gallery of {{ $query }}</h2>

				@foreach($results as $key => $item)
				  <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">
					<img class="attachment-full size-full" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';"  title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
				  </a>
				  <a target="_blank" href="{{ $item['url'] }}">.</a>

				@endforeach

              </div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>

        </div>
      </div>
      </div>

@include('sidebar')

@endsection
