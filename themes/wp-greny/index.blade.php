@extends('layout')

@section('content')

<div class="iklanpage">
{!! ads('responsive') !!}
</div>

<div class="postingan">

<div class="home_post_box">
<div class="infopost">
<h2><a href="{{ home_url() }}" title="{{ sitename() }} - {{ config('site.description') }}">{{ sitename() }} - {{ config('site.description') }}</a></h2>
</div>

@for($i=0; $i<=count($results)-1; $i++)
	<span class="realpict">
	<a rel="bookmark" href="{{ attachment_url( $query, $results[$i]['title'] ) }}">
		<img class="attachment-thumbnail size-thumbnail" width="98" height="98" src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';" title="{{ $results[$i]['title'] }}" alt="{{ $results[$i]['title'] }}" />
	</a>
	</span>
@endfor

  </div>
<div class="clear"></div>
<div class="relsing">
            <h2>Popular Post</h2>
			@foreach( $random_terms as $term )
             <a title="{{ $term }}" title="{{ $term }}" href="{{ permalink($term) }}">{{ ucwords($term) }}</a>,
            @endforeach
            <div class="clear"></div>
          </div>

      <div class="clear"></div>
  <div id="post-navigator">
  </div>
</div>

@include('sidebar')

@endsection
