<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">

<head>
  <meta charset="utf-8">
@include('header')

<style>
/*
Theme Name: WP Greny
Description: No Description, Auto Pageone!
Version: 1k
Author: {{ sitename() }}
*/

/* Global */
* { margin: 0; padding: 0; }
body { font-size: 12px; background-color:#222; font-family: "Open Sans",sans-serif; color: #999; }
a { color: #000; text-decoration: none; }
h1,h2,h3,h4,h5,p { margin: 10px 0; }
h1 { font-size: 22px; }
#main_container { background-color:#000; border: 10px solid #000; width: 898px; margin: 75px auto 30px;}
.clear { clear: both; }
.left { float: left; }
.right { float: right; }
img { border: none; } 

/* Header */
#head { background:#000; font-size:18px; padding:10px; border-bottom: 5px solid #00FA9A; width: 100%; position: fixed; z-index: 999; top: 0px;}
#header { width:918px; margin:0 auto;}
.logo { float: left; margin-top: 13px; }
#walldrift { color:#494949; font-size: 30px; font-family: arial; font-weight: bold; height:41px; margin-top: 0px; float: left; text-transform: capitalize;}
#walldrift a { color:#00FA9A;  text-transform: uppercase;}
#walldrift a:hover{ color:#3CB371;}
#walldrift h1 a{ color:#494949; font-size: 30px; font-family: arial; font-weight: bold; height:41px; margin-top: -10px; float: left; text-transform: capitalize; text-indent: -9000px;}
#walldrift h2 a{ color:#494949; font-size: 30px; font-family: arial; font-weight: bold; height:41px; margin-top: -10px; float: left; text-transform: capitalize; text-indent: -9000px;}
.menu{float:right;margin:5px 25px;}
.menu ul{height: auto; padding-left: 10px;}
.menu li{list-style:none; margin:2px;padding:5px 0px 5px 5px; display:inline-block;}
.menu a{color: #999;}
.menu a:hover{color: #333;}
.search{ -webkit-transition: all .5s; -moz-transition: all .5s; transition: all .5s; width: 4px; border:#000; background:#000 url(images/search-icon.png) no-repeat 2px center; padding: 5px 10px; float: left; font-family: calibri; font-weight: 200; font-size: 12px; color: #333;border-radius:5px;}
input.search:focus{width: 210px; background:#000; color:#fff;}

/* Content */
.postingan{ width: 728px; float: left; }
.home_post_box { width: 728px; /* Old Firefox */ float:left; margin-bottom: 10px;  position: relative; }
.mpret {padding:5px; text-align:left; line-height: 1.5em; font-size: 14px; box-shadow: 0 1px 3px rgba(0,0,0,.1); color: #717171;}
.infopost { margin-bottom: 5px;}
.infopost h2 { margin:0;}
.infopost h2 a { font-size: 20px; font-weight: 100; margin: 20px 0 0; color: #00FA9A; }
.infopost h2 a:hover { text-decoration:underline; }
.navigation .left, .navigation .right { padding: 10px 35px; background-color: #000; }
.navigation a:hover { color:#999; }
img.attachment-featured-home { width: 104px; height: 104px; margin: 0 0 6px 6px; }
img.attachment-featured-homes { width: 510px; height: 312px; float: left; margin: 6px 6px 0 0; }
#content_left { width: 728px; float: left; margin-bottom: 10px; font-size: 12px; font-family: arial; color: #999;} 

/*single content*/
.lead{font-size: 18px; color:#000; background: #3CB371; margin-top: -10px; padding: 5px;}
.htree{ background: #3c3c3c; margin-bottom: -10px; padding: 5px 5px 5px 10px; font-size: 12px; text-transform: uppercase;  font-family: arial;  color: #999; background-image: url('images/footerBG1.png');  background-position: left top;  background-repeat: repeat-x;  border-top: 1px solid #252525;  border-bottom: 1px solid #252525;  border-right: 1px solid #252525;  border-left: 1px solid #252525; }
.postgallery{text-align: center;}
.date { color: #018E6B; padding-bottom:5px; border-bottom:1px solid #018E6B;}
.singlet { width:728px; margin-top:5px;}
.singlet img.attachment-large { width:728px; height:auto; margin:0;}
.single_content { width:728px; }
.single_content h2 span {background: #3CB371; padding: 5px 10px;}
.single_content h2 { font-size: 16px; margin:20px auto 10px; color: #000;background: #3CB371; padding: 5px 10px;}
.single_content p { color: #999; text-align: justify; line-height: 1.5em; font-size: 13px; }
.single_content a { color: #999; }
.single_content img { width: 728px; margin-top: -50px; height: auto; cursor: pointer; }
img.attachment-large {width:98px; height:98px; margin:6px 3px 0 3px;}
img.attachment-full {width:95px; height:95px; margin:6px 3px 0 3px;}
.footer_inside_box { margin-bottom: 10px; }
.footer_inside_box a:hover { color:#999; }
.description_images{ width: 726px; background:#404040; border-top: 1px solid #474747; box-shadow: 0 1px 3px rgba(0,0,0,0.33); padding: 0 10px 2px 10px; }
.description_images p { text-align:justify; color: #999; }
.description_images a:link,.description_images a:visited { color:#777 }
.description_images h2{ text-align:justify; font-size: 12px; margin: 5px 0 -5px 0; text-transform: uppercase; font-family: arial;color: #999; }
.post { border: 1px solid #393939; }
.post h3 { margin-top: 0; font-size: 14px; margin:5px 10px; }
#images_details{ color:#999999;   padding: 5px 8px 1px 5px; font-size: 12px; margin-top: 0; border-top: 1px solid #4b4b4b;   border-right: 1px solid #252525;  border-left: 1px solid #252525; border-bottom: 1px solid #252525; background: rgb(51, 51, 51); height: 138px; }
#images_details a:link,#images_details a:visited {color:#777}
.detils-k{ width: 235px; float: left; padding: 2px; }
.detils-r{ float: left; width: 280px; padding-top: 7px; }
.rel{ margin-left: -5px; width: 760px; }
.relsing { font-size:20px; margin:20px 0 0; font-weight: 100; color: #3CB371; text-align: center;}
.relsing h2 span {background:#3CB371; padding:5px 10px;}
.relsing h2 { font-size:16px; margin:20px 0 10px; color: #000;background:#3CB371; padding:5px 10px; }
.relsing h3 { margin: 5px 30px 0 0; }
.relsing li { float: left; }
.relsing ul { margin: 10px 15px; }
.relsing a{ font-size:14px; color:#999; font-weight:100;}
.relsing a:hover{ color:#00FA9A; text-decoration:underline; }
.relsing p{ font-size:11px; color:#999; text-decoration:underline; font-family: arial;}
.breadchumb{background:#00FA9A; margin-top: -1px; margin-bottom:10px; font-size: 11px;}
#crumbs{ color:#222; padding:5px; }
.mprets {text-align:justify; line-height: 1.5em; font-size: 14px;}
.backtop {padding:10px; margin-top: 30px; width:auto; height:auto; overflow: hidden; font-weight:bold; font-family: calibri; border-top: 1px solid #4A4A4A;}
.backto a {color:#999;}
.backto { font-size: 14px; margin-top: 20px; font-weight: bold;}
.backto :hover {color:#777; text-decoration:underline;}
.teng {background:#F1F1F1; padding:10px;}

/* Navi Single */
.navsingle {padding:10px; margin-top: 30px; width:auto; height:auto; overflow: hidden; font-weight:bold; border-top: 1px solid #4A4A4A;}
.navprevious {max-width: 50%; float: left;}
.navnext {max-width: 50%; float: right;}
.navprevious a:hover {color: #777; text-decoration:underline;}
.navnext a:hover {color: #777; text-decoration:underline;}

/* attachment */ 
#attachment { width: 728px; float: left; box-shadow: 0 1px 2px #018E6B; }
.att_img img {width:728px; height:auto;}
.attachment_page { width:728px; float:left; }
.attachment_page p {color: #999; text-align: justify; line-height: 1.5em; font-size: 13px;}
.galleryatt { margin-left:0px; }
.galleryblok {text-align: center;}
.galleryblok h2 span {padding:5px 10px; background:#3CB371;}
.galleryblok h2{color: #000; font-size:16px; margin-top: 20px;padding:5px 10px; background:#3CB371;}
.attachment_page img { width:100%; height: auto; margin-bottom: 3px;}
.image-meta a { color: #cccccc; }
.image-meta a:hover { color: #777; text-decoration: underline; }
#submit { background-color: #000; color: #000; border: none; padding: 5px; }
img.attachment-thumbnail {width:95px; height:95px; margin: 6px 3px 0 3px;} 
.bawahgambar {height:90px;}
.backatt {background: url(images/prev.png) no-repeat 2px center; color:#000; width:110px; font-size:60px; text-align:center; float:left; margin:10px 0;}
.backatt a{color:rgba(255, 255, 255, 0); padding:0 40px;}
.nextatt {background: url(images/prev.png) no-repeat 2px center; color:#000; width:110px; font-size:60px; text-align:center; float:right; margin:10px 0;}
.nextatt a{color:#000;}

/*page*/
.single_page { padding:10px; }
.postinganpage{ width: 908px; float: left; }
.home_post_box_page { width: 444px; margin-right:10px; padding-top: 15px; /* Old Firefox */ float:left; margin-bottom: 10px;  position: relative; }
#page {background:#3CB371; width:100%; height:35px;}
.ragal {width:444px; height:250px; overflow:hidden; margin-left:-3px;}
.ragal img {width:444px; height: auto; margin-top:-20px;}
.ragaley {width:480px; height:65px; margin-top: 2px; overflow:hidden;}
.ragaley img {width:85.88px; margin:0px;}
.paged a { padding: 10px 15px; font-family:arial; font-size:14px; text-transform: uppercase; }
.paged a:hover {color:#000; text-decoration:underline;}
.paged {padding: 10px 0; border-right:2px solid #00FA9A; float:left;}
.randomtitleh2 {margin:5px 0; float:left; }
.randomtitleh2 a {color:#999; padding-left:10px; }
.randomtitleh2 a:hover {color:#3CB371;}
.randomtitleh2 h2 a {padding:0 5px 5px 0; color:#3CB371; font-weight:100; border-radius:5px; }
.randomtitleh2 h2 a:hover {  text-decoration:underline; }
.randomtitleh2 h2 {margin:0;}

/* Pagenavi */ 
#post-navigator{padding:0px;clear:both;height:auto;width:100%;display:block;margin-top:1em;margin-right:auto;margin-bottom:1em;margin-left:auto;font-size:10px;}
.wp-pagenavi{width:100%;clear:both;padding-top:0.5em;padding-right:0px;padding-bottom:0.5em;padding-left:0px;height:auto;text-align:center;margin-top:0px;margin-right:auto;margin-bottom:0px;margin-left:auto;}
.pageback{color:#3CB371; padding:10px;}
.pages{background:#3CB371;color:#FFF;padding-top:5px;padding-right:8px;padding-bottom:5px;padding-left:8px;width:auto;height:auto;margin-top:0px;margin-right:3px;margin-bottom:0px;margin-left:0px;border: 1px solid #3CB371;}
.current{color:#CCC;width:auto;height:auto;margin:0px;padding-top:5px;padding-right:8px;padding-bottom:5px;padding-left:8px;border:1px solid #3CB371;}
.wp-pagenavi a{color: #999;text-decoration:none;height:auto;width:auto;margin:0px;padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;display:inline;border: 1px solid #3CB371;}
.wp-pagenavi a:hover{background:#3CB371;color:#FFF;border:1px solid #3CB371;}
#post-navigator .alignleft a, #post-navigator .alignright a{color:#777;margin:0px;height:auto;width:auto;padding-top:5px;padding-right:5px;padding-bottom:5px;padding-left:5px;text-decoration:none;display:block;background-color:#000000;} #post-navigator .alignleft a:hover, #post-navigator .alignright a:hover{color:#000111;text-decoration:none;background-color:#000000;}

/* Sidebar */
#side { width: 160px; float: right; margin-top: -10px; }
#sidebar { width: 250px; background: #000; font-size: 14px; box-shadow: 0 1px 2px #018E6B;}
.cat{ padding: 10px; width: 280px; }
.cat ul { list-style-type: none; margin-left: 0px; }
.cat ul a { color: #999; font-size: 12px; font-size: 12px; }
.cat ul a:hover { color: #999; text-decoration: underline; }
.cat ul li { margin-bottom: 8px; margin-right:5px; display: inline-block; width: 110px; }
.cat li { padding: 5px 0; }
.cat li a { color: #999; font-size: 14px; }
.cat li a:hover { color: #018E6B; text-decoration: underline; }
.cat_last { border-right: none; }
.hcat{margin:10px 0 3px; font-weight:bold; padding: 0 0 5px; font-size: 14px; text-transform: uppercase;  font-family: verdana; color: #00FA9A; border-bottom: 1px solid;}
.popularpost { width:160px;}
.home_post_thumb{ width: 100px; float: left; }
.infothumb a{ float: left; width:155px; padding:3px 0px; font-size: 13px; text-decoration: none; cursor: pointer; font-family: arial; color: #00FA9A; font-weight:100;  }
.infothumb a:hover { text-decoration: underline; padding-left:5px; color: #000; background:#3CB371;}
.infothumb p{ float: left; font-size: 10px; font-family: arial; font-weight: bold; margin: 0 0 0 5px; color: #777; }

/* Footer */ 
#footer { background: #000; margin-top: 15px; padding: 5px 0; border-top:solid 2px #00FA9A;}
.footer_box { width: 1018px; margin: 10px auto 0; }
.bottom_footer { width:1018px; margin:0 auto; color:#999; text-align: right; padding: 10px 10px; font-size: 11px; text-align: center;}
.bottom_footer a { color:#999; text-transform: capitalize;}
.disclaimer p{ text-align: justify; font-style: italic; color: #999; font-size: 12px; border-left: 3px solid #999; padding-left: 5px; position: relative; }
.ontop { float:right; margin-top: -35px; background: #494949; width: 72px; height: 22px; padding: 5px 0 0 15px; border-radius: 5px; box-shadow: 0 1px 2px #018E6B;}
.ontop:hover, .ontop a:hover{ cursor: pointer; }
.ontop a{ color: #000; font-size: 10px; padding-top: 5px; }
#menu-wallpaper .menu{ margin-top: 15px; padding: 5px 23px 15px; background-color: #0B0B0B; border: 1px solid #393939; -moz-box-shadow: 0 1px 3px #0B0B0B; /* Old Firefox */box-shadow: 0px 1px 3px #0B0B0B; }
.download .down{ float: right; background: #424242; padding: 5px 10px; border-radius:3px;  cursor:pointer; margin: 0 5px; text-transform: uppercase; text-shadow: 0 1px 2px #0a0a0a; }
.fullsize .full{ float: right; background: #424242; padding: 5px 10px; border-radius:3px;  cursor:pointer; margin: 0 5px; text-transform: uppercase; text-shadow: 0 1px 2px #0a0a0a; }
.download .down:hover{ background: #494949;   cursor:pointer; }
.fullsize .full:hover{ background: #494949;   cursor:pointer; }
.tombol{ float: left; width: 200px; }
.adspost{ width: 735px; height: 95px; background-color: rgb(51, 51, 51); margin-top: 7px; border-left: 3px solid;  margin-left: 7px; padding: 5px 0 0 5px; }
.adside{ margin: 10px auto 0; }
.iklan{ margin: 5px auto 10px; }
.ikland{ margin: 0px auto 5px; }
.iklans{ margin: 10px auto 0; width:336px; height:280px;}
.iklanatt{ margin: 0px 0px 5px; }
.iklanpage{ margin: 0 auto 10px; padding:15px; }

/* mobile */
@media screen and (max-width: 950px) {
#main_container {width: 96%;}
#header {width: 96%;}
.bottom_footer {width: 96%;}
.postinganpage {width: 100%;}
.postingan {width: 100%;}
.home_post_box {width: 100%;}
#content_left {width: 100%;}
.single_content {width: 100%;}
.singlet {width: 100%;height:auto;}
img.attachment- {width: 100%;margin:0;}
#attachment {width: 100%;}
img.attachment-895x524 {width: 100%;}
.attachment_page {width: 100%;}
#side {width: 100%; text-align:center;}
.infothumb a {width: 100%; text-align:center;}
.popularpost { width: 100%; }
}

@media screen and (max-width: 900px) {
#content_left {width: 100%;}
.postingan {width: 96.5%;}
}

@media screen and (max-width: 907px) {
#main_container {width: 96%;}
#header {width: 96%;}
#page {background:none;box-shadow:none;}
.paged {background:#3CB371;}

}

@media screen and (max-width: 832px) {
#main_container {width: 96%;}
#header {width: 96%;}
.postingan {width: 96.5%;}
.home_post_box {width: 96.5%;}
}

@media screen and (max-width: 790px) {
#main_container {width: 96%;}
#header {width: 96%;}
#content_left {width: 100%;}
.single_content {width: 100%;}
.singlet {width: 100%;height:auto;}
img.attachment- {width: 100%;margin:0;}

}

@media screen and (max-width: 704px) {
img.attachment-featured-homes {width: 100%;height:auto;margin-bottom:10px;}
}

@media screen and (max-width: 584px) {
img.attachment-featured-homes {width: 100%;height:auto;}
}

@media screen and (max-width: 450px) {
img.attachment-featured-homes {width: 100%;height:auto;}
img.attachment-895x524 {width: 98%;margin:0;}
}	
</style>

</head>

<body>

<div id="head">
  <div id="header">
    <div id="walldrift">
      <a href="{{ home_url() }}">{{ sitename() }}</a>
    </div>
    
  </div>
</div>
<div class="clear"></div>

<div id="main_container">
	<div class="clear"></div>

@yield('content')

    </div>
	
    <div id="footer">
      <div class="bottom_footer">
			© 2017 {{ sitename() }}
		  <br>Powered by {{ sitename() }}
		  <br>Sitemap: 
		  @foreach( range('a', 'z') as $abjad)
			<a href="{{ permalink($abjad, 'list_term') }}">{{ $abjad }}</a> 
		  @endforeach
		</div>
    </div>
</div> 

	@include('footer')
  </body>
</html>