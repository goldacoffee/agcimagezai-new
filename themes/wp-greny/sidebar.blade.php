<div id="side">

<div class="adside">
{!! ads('responsive') !!}
</div>

<div class="hcat">Popular</div>

<div class="popularpost">

@foreach( array_slice($random_terms, 0, 25) as $term )
<div class="pop">
<div class="infothumb"><a href="{{ permalink($term) }}" class="">{{ ucwords($term) }}</a></div>
</div>
@endforeach

</div>
</div>
<div class="clear"></div>