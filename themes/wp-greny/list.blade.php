@extends('layout')

@section('content')

<h1 class="lead">{{ $list_title }}</h1>

<div class="pageback randomtitleh2">
	@foreach( $random_terms as $term )
		<a title="{{ ucwords($term) }}" href="{{ permalink($term) }}">{{ ucwords($term) }}</a><br>
	@endforeach
<div class="clear"></div>
<div id="post-navigator">
</div>
</div>

<div class="clear"></div>

@endsection
