@extends('layout')

@section('content')

<div class="breadchumbatt">
			<div class="crumb">
        <a href="{{ home_url() }}">Home</a> &raquo; <a href="{{ permalink( $query ) }}">{{ $query }}</a> &raquo; <a href="{{ get_permalink() }}">{{ $subquery }}</a> » <span class="current1">{{ $subquery }}</span>
      </div>
</div>
<div class="clear"></div>


<div class="attachment">
						<div id="post">

              <h1>{{ $subquery }} - {{ $query }}</h1>

{!! ads('responsive') !!}

              				<div class="clear"></div>
            </div>

<a href="{{ attachment_url( $query, $gallery['current']['title'] ) }}" title="{{ $gallery['current']['title'] }}" rel="attachment">

  <img style="width:100%" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" class="attachment-895x524" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}">

</a>
<div class="clear"></div>
<h2><center>{{ $gallery['current']['title'] }}</center></h2>
{!! ads('responsive') !!}


<div class="magc-attachmentinfo">
				<p><strong>{{ $gallery['current']['title'] }}</strong> is part of {{ $query }}. <em>{{ $gallery['current']['title'] }}</em> was created by combining fantastic ideas, interesting arrangements, and follow the current trends in the field of that make you more inspired and give artistic touches. We'd be honored if you can apply some or all of these {{ $gallery['current']['title'] }} in your need. believe me, brilliant ideas would be perfect if it can be applied in real and make the people around you amazed!</p>
				<p><u>{{ $gallery['current']['title'] }}</u> was posted in {{ date('d/m/Y') }}. {{ $gallery['current']['title'] }} has viewed by {{ rand(10,999) }} users. Click it and download the <strong>{{ $gallery['current']['title'] }}</strong>.</p>
</div>

<div class="clear"></div>

<div class="attacmentgallery">
				<h3>Gallery of : {{ $subquery }}</h3>

		<style type="text/css">
			#gallery-1 {
				margin: auto;
			}
			#gallery-1 .gallery-item {
				float: left;
				margin-top: 10px;
				text-align: center;
				width: 16%;
			}
			#gallery-1 img {
				border: 2px solid #cfcfcf;
			}
			#gallery-1 .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>
		<div id="gallery-1" class="gallery galleryid-6386 gallery-columns-6 gallery-size-thumbnail">


      @foreach($results as $i => $item)

      <dl class="gallery-item">
			<dt class="gallery-icon landscape">
				<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}" rel="bookmark">
        <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-thumbnail" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
        </a>
			</dt>
    </dl>

      @endforeach

		</div>

							</div>

</div>



@endsection
