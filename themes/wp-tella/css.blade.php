<style>
/* Global */
* {margin: 0; padding: 0;}
body {font-size: 12px; color:#bfbfbf; font-family: arial, georgia; background: #363636; }
.left {float: left;}
.right {float: right;}
.clear {clear: both;}
a{color:#a0a0a0; text-decoration:none;}
a:hover{text-decoration:underline; color:#FF1493;}
strong a{text-decoration:none; color:#a0a0a0;}
strong a:hover{text-decoration:none; color:#FF1493;}
h1,h3, h5 { margin:10px 0;}
p {margin:0px 0;}
h2{margin:5px 0;}
h4{margin:5px 0 0 5px; font-size:13px; color:#FF1493;}

/* Header */
#container{width: 918px; margin: 0 auto; margin-top: 10px; margin-bottom: 10px; background-color: #292929; border:3px #151515 solid; border-radius: 12px;}
.magc-head{background: #1f1f1f font-family: arial; padding: 10px; border-bottom:2px solid #FF1493;}
.magc-blogname a{font-size: 28px; font-family: arial; color:#FF1493; padding:0px 10px 10px 0px; text-decoration:none; font-weight: bold; text-shadow: 0 1px 2px #0a0a0a;}
.magc-blogtitle{float: left; margin-top: 5px;}
.magc-menu{float:right;}
.magc-menu ul{height: auto; padding-right: 10px; padding-left: 10px;}
.magc-menu li{list-style:none; margin:2px; padding:5px; display:inline-block;}
.magc-menu a{color: #a0a0a0;}
.magc-menu a:hover{color: #FF1493;}
.carian {float:right; padding: 10px 10px 0px 0px;}

/* Content */
#content{width: 937px;margin: 0 auto; margin-top: 20px;padding: 0 20px;}
#wrap {width: 728px; float:left; }
.title h1{font-size: 14px; margin: 20px; padding: 5px; font-family: arial; text-align: center; border-bottom: solid 1px #a0a0a0;}
.magc-titleerror{font-size: 15px; margin-bottom: 5px; margin-top: 0; padding: 8px; font-family: arial;}
.magc-post {float:left; margin-bottom: 10px; position: relative; font-family: arial; border-bottom: solid 1px #a0a0a0;}
.magc-post h2{text-align: center; color:#FF1493;}
.magc-post h3{text-align: center; margin-top: -3px; color:#a0a0a0; font-size: 12px; font-weight: normal;}
.magc-post h3 a{color:#a0a0a0;}
.magc-post a{text-decoration: none; color:#FF1493;}
.magc-post a:hover{text-decoration: underline; color:#FF1493;}
.magc-post p a{text-decoration: none; color:#a0a0a0;}
.magc-postimg { float:left; width:302px; height:200px;}
.magc-postimg img{ opacity: .9;}
.magc-postimg img:hover{ opacity: 1.0;}
.magc-postlittleimg{ float: left; width: 204px; height:203px; padding-right:5px;}
.magc-postlittleimg img{ opacity: .9;}
.magc-postlittleimg img:hover{ opacity: 1.0;}
.magc-postinfo {float: right; /*margin:10px 5px;*/ font-size: 12px; font-weight: normal; width:215px; height:relative;}
.attachment-featured-thumb{float:left; width:100px; height:99px; padding:1px; }
.attachment-featured-home{float:left; width:300px; height:200px; padding:1px; }

/* Single */
.magc-postsingle {background: #292929; width: 728px; float: left;}
.magc-postsingle h1 {padding:0px 10px; color:#FF1493; font-size: 14px; font-family: 'verdana'; font-weight:bold;}
.magc-postsingle h2 {text-align: left; color:#a0a0a0; padding:0px 10px; font-size: 13px; font-family: arial;}
.magc-postsingle p {text-align: center; font-size: 13px;}
.attachment-large {width: 728px; height: auto; float:center; opacity:1.0; filter:alpha(opacity=100);}
.magc-postgallery{/* Old Firefox */ position: relative; font-family: arial; margin: 20px;}
.magc-postgallery img{ opacity: .9;}
.magc-postgallery img:hover{ opacity: 1.0;}
.magc-gallerytitle{font-size: 14px; font-family: arial; color: #a0a0a0; letter-spacing: 1px;}
.attachment-full{float: left; margin:2px; width: 90px; height: 80px; border: 2px #1f1f1f solid; border-radius: 0px;}

/* Related Post*/
.magc-postrel {background: #292929; width: 728px; float: left;}
.magc-titlerel {font-size: 14px; font-family: arial; color: #a0a0a0; letter-spacing: 1px; padding-top:10px; margin: 5px;}
.magc-postboxrel {background: #292929; width: 728px; margin: 5px; padding:3px; float:left; position: relative; font-family: arial; border:1px solid #a0a0a0; border-radius: 5px;}
.magc-postboximgrel { float:right; background: #1f1f1f; text-align: justify; padding: 5px; border-radius: 5px;}
.magc-postboximgrel img{ opacity: .9;}
.magc-postboximgrel img:hover{ opacity: 1.0;}
.magc-postinforel{float:left; padding: 5px; width: 360px; }
.magc-postinforel h3 a{font-size: 15px; text-decoration: none; cursor: pointer; color: #FF1493;}
.magc-postinforel h3 a:hover{text-decoration: underline; cursor: pointer; color: #FF1493;}
.magc-postinforel p{text-align: justify; font-family: arial; font-weight: normal; color: #a0a0a0;}

/*attachment*/
.breadchumbatt{color:#a0a0a0; font-size: 12px; padding: 10px 10px 0 10px; font-family: arial;}
.breadchumbatt a{color:#a0a0a0; text-decoration: none;}
.breadchumbatt a:hover{color:#FF1493; text-decoration: underline;}
#contentatt{width: 918px; margin: 0 auto;}
.attachment{padding-bottom: 10px; float:left; font-family: arial;}
.attachment h1 {margin:10px; color:#FF1493; font-size: 14px; font-family: arial;}
.attachment-895x524{width:918px; height: auto; float:left;}
.attachment h2 {margin:10px;font-size: 13px; font-family: arial;}
.magc-attachmentinfo{margin:10px;font-family: arial; color: #a0a0a0; text-align: justify;}
.attachmentinfo2{margin:10px;font-family: arial; color: #a0a0a0;}
.attachment-thumbnail{width:130px; height: 100px; float:left; margin:-10px 4px 5px 22px; margin-bottom:10px; border-radius: 5px;}
.attacmentgallery h3{margin: 10px 20px;}
.attacmentgallery img{opacity: .9;}
.attacmentgallery img:hover{ opacity: 1.0;}

/* Breadchumb */
.breadchumb{color:#a0a0a0; font-size: 12px; padding: 10px 10px 0 10px; font-family: arial;}
.breadchumb a{color:#a0a0a0; text-decoration: none;}
.breadchumb a:hover{color:#FF1493; text-decoration: underline;}

/* Sidebar */
#sidebar {width: 160px; background: #292929; float: right; padding:10px; text-shadow: 0 1px 5px #292929; font-family: arial;}
.magc-sidebarbox { background: #292929;margin-bottom: 10px; padding: 10px;}
.magc-sidebarname{font-family: arial; text-align:center; padding:5px; background-color:#181818; font-size: 13px; color:#bfbfbf; margin-top:0px; border-bottom: solid 1px #FF1493;}
.magc-sidebarbox a {color: #bfbfbf; text-decoration: none; font-size:12px; font-weight:bold;}
.magc-sidebarbox a:hover {color:#FF1493;font-weight:bold;}
.magc-sidebarbox li {list-style: none;}
.magc-sidebarbox p{font-size:11px; text-decoration: none; color:#FF1493;font-weight:normal;}
.magc-sidebarbox img{ opacity: .9;}
.magc-sidebarbox img:hover{ opacity: 1.0;}
.sidebarimg{margin: 1px;}
.cat-item {padding-bottom: 10px;}
.search{background: #ffffff url(images/search-icon.png) no-repeat 115px center; width: 125px; color:#1f1f1f; padding: 7px; border: solid 1px #dadada; border-radius: none; -webkit-border-radius: 3px; -moz-border-radius: 3px; -o-border-radius: 3px; marginbottom: 7px;}

/*Page Navi Parsing*/
.wp-pagenavi{width: auto; text-align:center; margin-bottom:20px; margin-top:30px; clear:both;}
.wp-pagenavi a, .wp-pagenavi span{text-decoration:none; font-weight:bold; border:2px groove #555; padding:3px 5px; margin:2px; color:#a0a0a0;}
.wp-pagenavi a:hover, .wp-pagenavi span.current{color: #a0a0a0; border:2px groove #FF1493; padding:3px 5px;}
.wp-pagenavi span.current{font-weight:bold; border:2px groove #FF1493; padding:3px 5px;}

/* Page */
.magc-postpage{ background: #292929; padding: 10px; float: left; font-family: arial;}
.magc-titlepage h1{color:#FF1493; font-size: 16px;}
.magc-postpage p {text-align: justify; font-size: 14px;}
.magc-postpage li {list-style: circle; margin: 0 20px; padding: 5px 0; text-align: justify; font-size: 14px;}
.magc-postpage strong{padding-top: 10px;letter-spacing: 1px;color:#FF1493;}
.email {text-transform: lowercase;}
.magc-searchpage{margin-top: 10px;}

/* Navi Single */
.navsingle {background: #292929; border-radius:20px; border:1px solid #a0a0a0; padding:10px; margin:5px; max-width:100%; height:auto; overflow: hidden; font-weight:bold;}
.navprevious {max-width: 50%; float: left;}
.navnext {max-width: 50%; float: right;}
.navprevious a, .navnext a {text-decoration:none; color: #FF1493;}
.navprevious a:hover, .navnext a:hover {text-decoration:underline; color: #FF1493;}

/* Footer */
#footer {width: 918px; margin-top: 5px; padding: 20px 0; font-size: 11px; font-style: normal; text-align:center; font-family: arial; border-top:2px solid #a0a0a0;;}
.magc-footer a {text-align: justify; text-decoration: none;}
.magc-footer a:hover {text-decoration:underline;}
</style>
