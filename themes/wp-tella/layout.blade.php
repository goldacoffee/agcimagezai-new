<!DOCTYPE html>
<html lang="en">
<head>

@include('header')

@include('css')

</head>
<body>
	<div id="container">
	    <div class="magc-head">
	        <div class="magc-blogname">
				<a class="magc-blogtitle" href="{{ home_url() }}">{{ sitename() }}</a>
			</div>

			<div class="magc-menu">
			</div>
		<div class="clear"></div>
		</div>

@if( !is_attachment() )

	<div id="wrap">
	@yield('content')
	</div><!-- end wrap -->
	@include('sidebar')

@else

@yield('content')

@endif
		<div class="clear"></div>
		<div id="footer">
			<div class="magc-footer">
        @foreach( config('themes.page') as $page )
				    <a href="{{ permalink( $page, 'page') }}">{{ ucwords($page) }}</a> |
        @endforeach
				<a href="{{ home_url('sitemap.xml') }}">Sitemap</a>
			<br/>
			<div class="magc-footer">{{ sitename() }} &copy; {{ date('Y') }}. All Rights Reserved.</div>
		</div><!-- footer -->
	</div><!--   end container   -->

  @include('footer')

</body>
</html>
