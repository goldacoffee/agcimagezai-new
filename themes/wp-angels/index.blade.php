@extends('layout')

@section('content')

@php
	$cols   = array_group( $results, 5 );
@endphp

<h1 itemprop="headline">Welcome to {{ sitename() }}</h1>
<div class="adr_contentfront">

@foreach ($cols as $item)

	@php
		$lastitem = end($item);
	@endphp

<article class="LatestPost">
 <div class="randomxhouse">
	<div class="picxhouse">
		<a href="{{ attachment_url( $query, $lastitem['title'] ) }}" rel="bookmark" title="{{ ucwords($query) }} {{ ucwords( $lastitem['title'] ) }}">
			<img src="{{ $lastitem['small'] }}" data-src="{{ $lastitem['url'] }}" onerror="this.onerror=null;this.src='{{ $lastitem['small'] }}';" class="attachment-featured-home size-featured-home wp-post-image" alt="{{ ucwords($query) }} {{ ucwords( $lastitem['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords( $lastitem['title'] ) }}" width="280" height="150" />
		</a>


	   <div class="imgallery" style="display: inline-flex;margin-top: 5px;">

		@for( $i=0; $i<=count($item)-2; $i++ )

		  <a href="{{ attachment_url( $query, $item[$i]['title'] ) }}" rel="bookmark" title="{{ ucwords($query) }} {{ ucwords( $item[$i]['title'] ) }}" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
			<img width="70" height="55"  src="{{ $item[$i]['small'] }}" data-src="{{ $item[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $item[$i]['small'] }}';"  class="attachment-featured-random size-featured-random" alt="{{ ucwords($query) }} {{ ucwords( $item[$i]['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords( $item[$i]['title'] ) }}"/>
		  </a>

		@endfor

		</div>

	   <div class="kyj_title">
		  <h2 itemprop="headline"><a href="#" title="{{ ucwords( $lastitem['title'] ) }}" rel="bookmark">{{ ucwords( limit_the_words( $lastitem['title'], 5 ) ) }}</a></h2>
	   </div>
	</div>
 </div>
</article>

@endforeach

  <div class="cl"></div>

  <ul class="randomindex">
	 @foreach($random_terms as $term)
		<li class="kyjmedialink">
			<a title="{{ ucwords($term) }}" href="{{ permalink($term) }}" >{{ ucwords($term) }}</a>
		</li>
	 @endforeach
  </ul>

	@php
	  $pagination = range(1,20)
	@endphp

      <div id="post-navigator">
         <div class="wp-pagenavi">
			 <span class="pages">Page 1 of {{ end($pagination) }}:</span>

         @foreach($pagination as $n => $page)
            @if( input()->get('page') == $n+1 )
                <strong class='current'>{{ $page }}</strong>
            @elseif( $page == end($pagination) )
                <a href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
            @else
                <a href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
            @endif
         @endforeach

		</div>
       <div class="cl"></div>
      </div>

</div>

@endsection
