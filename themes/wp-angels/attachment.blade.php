@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('content')



@include('breadcrumb')

<div id="adr_contentfront">
   <div id="leftxhouse">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
         <h1 itemprop="headline" class="headxhouse">{{ $query }}</h1>
		 @yield('ads')
         <div class="xhouseimg" itemprop="articleBody">
					  <img width="1187" height="780" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" class="attachment-full size-full" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}">
         </div>
		 @yield('ads')
		 <div class="rads">
			 <div class="nextat"><a href="{{ attachment_url( $query, $gallery['next']['title'] ) }}">Next Image</a></div>

			 <div class="prevat"><a href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}">Previous Image</a></div>
		 </div>

         <div class="cl"></div>
         <div class="xhousecontent">
            <div itemprop="articleBody">

<div class="xhousecontent">
   <h2>{{ $subquery }}</h2>
   <br>

   <p>Above photo is <strong>{{ $subquery }}</strong> posted by <i><a href="{{ home_url() }}" title="{{ sitename() }}">{{ sitename() }}</a></i> on {{ date('d/m/y') }}. The picture was taken and seen by 3 User and has been downloaded and reviewed {{ rand(100,1000) }} Times. You can use the image as background for your computer desktop and laptop screen, because this image has a size of has {{ $gallery['current']['size'] }} Pixel. If you want to save to a personal computer, you can download this image in full size. Please click "Download picture in full size" at the end of the article, then, your device automatically saves these images into a directory on your disk space. If you want to download more images from our collection, please click on the image below and do the same steps, the full size download.</p>
   <br>
   <p>Change the picture in the color display with <strong>{{ $subquery }}</strong> is the right thing to get rid of your {{ $query }} on desktop computers, pc, computer, laptop, you use every day to work or just plain, perform daily activities. An image has an aura, which penetrates the feeling of someone, for example images, sees that motivation by you evoke the image then the image that there was joy, and even images, feelings of sadness to create.</p>
   <br>
   <p>Well we have collected this time some ideas work, can change the atmosphere of your Office or computer screen. You can several downloaded and stored in the computer's memory, or you can download all photos from our website free of charge. If you {{ $query }} to download, you should now work in full-screen mode, see this article, until the end of.</p>
   <br>
   <p></p>
   <p><strong>Detail pictures for {{ $subquery }}</strong></p>
   <br>
   <table class="table table-striped" style="font-size: 12px;">
      <thead>
         <tr>
            <th>No</th>
            <th>Image atribute</th>
            <th>Value</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td>1</td>
            <td>Title</td>
            <td>:{{ $subquery }}</td>
         </tr>
         <tr>
            <td>2</td>
            <td>Upload by</td>
            <td>:Admin</td>
         </tr>
         <tr>
            <td>3</td>
            <td>Upload date</td>
            <td>:{{ date('d/m/y') }}</td>
         </tr>
         <tr>
            <td>4</td>
            <td>Image link</td>
            <td>:<a href="{{ $gallery['current']['url'] }}" title="{{ $gallery['current']['title'] }}">Large</a>, <a href="{{ $gallery['current']['small'] }}" title="{{ $gallery['current']['title'] }}">Small</a></td>
         </tr>
         <tr>
            <td>5</td>
            <td>Size</td>
            <td>:{{ $gallery['current']['size'] }}</td>
         </tr>
         <tr>
            <td>6</td>
            <td>Copyright</td>
            <td>:{{ $gallery['current']['copy'] }}</td>
         </tr>
      </tbody>
   </table>
   <p></p>
</div>


            </div>

            <div class="cl"></div>
         </div>

		<div class="lbox">
		<div class="xhousecl">
		   <h3 class="xhouse_up" itemprop="headline">Gallery of {{ $query }}</h3>
				@foreach($results as $i => $item)
				<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}" rel="bookmark" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject" >
				  <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
				</a>
				@endforeach
		</div>
		<div class="cl"></div>
		</div>
      </article>
   </div>
   <aside id="xhouseside" role="complementary" itemscope="" itemtype="http://schema.org/WPSidebar">
      <div class="sidebarbox">
         <div class="ads_sidebar">
            <center>@yield('ads')</center>
         </div>
         <h3 class="xhouse_up">Popular Post</h3>
         <div class="postlist">

			 @foreach($random_terms as $term)
				<div class="popularlink">
					<h4><a title="{{ ucwords($term) }}" href="{{ permalink($term) }}" rel="bookmark">{{ ucwords($term) }}</a></h4>
				</div>
			 @endforeach

         </div>
      </div>
   </aside>
   <div class="cl"></div>
</div>

@endsection
