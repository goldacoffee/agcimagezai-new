@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('content')

@include('breadcrumb')

<div id="adr_contentfront">
   <div id="leftxhouse">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
         <h1 itemprop="headline" class="headxhouse">{{ $query }}</h1>
         <div class="xhouseimg" itemprop="articleBody">
            <div class="sdkonten" style="background: #3E3B3A;">
			@yield('ads')
               <figure>
                  <a href="#" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject" rel="bookmark">
					  <img width="1187" height="780" src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" class="attachment-full size-full" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}">
				  </a>
                  <figcaption>
                     <h2>{{ $results[0]['title'] }}</h2>
                     <figcaption> </figcaption>
                  </figcaption>
               </figure>
            </div>
         </div>
         <div class="cl"></div>
         <div class="xhousecontent">
            <div itemprop="articleBody">
                @include('spintax/search-2')
            </div>
			@yield('ads')
            <div class="prevnext">
               <div class="prev">&laquo; <a href="{{ permalink( $random_terms[0] ) }}" rel="prev">{{ ucwords($random_terms[0]) }}</a></div>
               <div class="next">&raquo; <a href="{{ permalink( end($random_terms) ) }}" rel="next">{{ ucwords( end($random_terms) ) }}</a></div>
               <div class="cl"></div>
            </div>
            <div class="cl"></div>
            <div class="xhousecl">
               <h3 class="xhouse_up" itemprop="headline">Gallery of {{ $query }}</h3>
					@foreach($results as $i => $item)
					<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}" rel="bookmark" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject" >
					  <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
					</a>
					@endforeach
            </div>
            <div class="cl"></div>
         </div>
      </article>
   </div>
   <aside id="xhouseside" role="complementary" itemscope="" itemtype="http://schema.org/WPSidebar">
      <div class="sidebarbox">
         <div class="ads_sidebar">
            <center>@yield('ads')</center>
         </div>
         <h3 class="xhouse_up">Popular Post</h3>
         <div class="postlist">

			 @foreach($random_terms as $term)
				<div class="popularlink">
					<h4><a title="{{ ucwords($term) }}" href="{{ permalink($term) }}" rel="bookmark">{{ ucwords($term) }}</a></h4>
				</div>
			 @endforeach

         </div>
      </div>
   </aside>
   <div class="cl"></div>
</div>

@endsection
