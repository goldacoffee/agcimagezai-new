<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="http://schema.org/Article" itemid="http://stoneangels.us" xmlns:x2="http://www.w3.org/2002/06/xhtml2" dir="ltr" lang="en-US">

   <head>
	  @include('header')
	  <link href="https://fonts.googleapis.com/css?family=Allerta" rel="stylesheet">
	  <link href="{{ theme_url('style.css') }}" rel="stylesheet">
   </head>

   <body id="blog" itemscope itemtype="http://schema.org/WebPage">
      <header id="kyj_header" id="site-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">


				
         <div class="h1_head">
            <div class="h1_header">
               <h1 id="logo" itemprop="headline"><a href="{{ home_url() }}" title="{{ config('site.description') }}">{{ sitename() }} | {{ config('site.title') }}</a></h1>
            </div>
            <div class="pop_right"><a href="{{ home_url('sitemap-image.xml') }}">Popular Image</a></div>
         </div>
      </header>
      <div id="kyj_container">
         <div class="cl"></div>
         <div id="midxhouse">
            <div id="kyj_midcontent">

			@yield('content')

            </div>
            <div class="cl"></div>
         </div>
      </div>
      <div id="foo_xhouse">
         <footer class="foo_xhouse-mid" id="site_footer" role="contentinfo" itemscope itemtype="htpp://schema.org/WPFooter">
            <div class="menu-footer-menu-container">
               <ul id="menu-footer-menu" class="menu">
					@foreach( config('themes.page') as $page )
						<li><a rel="nofollow"href="{{ permalink($page, 'page') }}">{{ ucwords($page) }}</a></li>
					@endforeach
					<li><a href="{{ home_url('sitemap.xml') }}">Sitemap</a></li>
               </ul>
            </div>
            <div class="cl"></div>
            <blockquote>
               <p class="dc">The rights of images, videos, and other materials found in this website, that's not stoneangels.us's property, remains to its respective owner/s.</p>
            </blockquote>
            <div class="foo_xhouse-copy">Copyright &copy; 2017 {{ sitename() }}. All Rights Reserved.</div>
               <div class="cl"></div>
         </footer>
         </div>
      </div>

	@include('footer')

   </body>
</html>
