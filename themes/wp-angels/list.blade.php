@extends('layout')

@section('content')
	  
<h1 itemprop="headline">{{ $list_title }}</h1>
<div class="adr_contentfront">
	
  <div class="cl"></div>
  
  <ul class="randomindex">
	 @foreach($random_terms as $term)
		<li class="kyjmedialink">
			<a title="{{ ucwords($term) }}" href="{{ permalink($term) }}" >{{ ucwords($term) }}</a>
		</li>
	 @endforeach
  </ul>

      <div id="post-navigator">
         <div class="wp-pagenavi">			 
         @foreach( range('a', 'z') as $abjad)
                <a href="{{ permalink($abjad, 'list_term') }}">{{ $abjad }}</a>
         @endforeach
		
		</div>
       <div class="cl"></div>
      </div>
	  
</div>
			   
@endsection