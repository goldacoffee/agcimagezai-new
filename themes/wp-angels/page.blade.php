@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('content')

<div id="adr_contentfront">
   <div id="leftxhouse">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
         <h1 itemprop="headline" class="headxhouse">{{ $page_title }}</h1>

         <div class="cl"></div>
         <div class="xhousecontent">
            <div itemprop="articleBody">
               <div class="xhousecontent">
                @include('page/' . strtolower($page_title) )
            </div>
            <div class="cl"></div>
         </div>
      </article>
   </div>

   <aside id="xhouseside" role="complementary" itemscope="" itemtype="http://schema.org/WPSidebar">
      <div class="sidebarbox">
         <div class="ads_sidebar">
            <center>@yield('ads')</center>
         </div>
      </div>
   </aside>

   <div class="cl"></div>
</div>

@endsection
