@extends('layout')


@section('content')

<header class="main-header">
<div id="header">
		<h1 class="title single-title">{{ config('site.title') }} - {{ config('site.description') }}</h1>
</div>
</header>
@include('breadcrumb')
		<div class="homead">
			{!! ads('responsive') !!}
		</div>

									<div class="dashedpost"></div>

@foreach( $results as $i => $item )
	<article class="pexcerpt0 post excerpt ">
		<a href="{{ attachment_url( $query, $item['title'] ) }}" rel="bookmark" title="{{ $item['title'] }}" id="featured-thumbnail">
		<div class="featured-thumbnail">
		<img src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';"  class="attachment-full size-full" alt="{{ $item['title'] }}" title="{{ $item['title'] }}">
		</div>
		<div class="featured-cat">☐ {{ $item['size'] }} pixel</div></a>
		<header>
		<h2 class="title">
		<a href="{{ $item['url'] }}" title="{{ $item['title'] }}" rel="bookmark">{{ limit_the_words($item['title'],4) }}</a>
		</h2>
		</header>
	</article>
@endforeach


		<div class="dashedpost"></div>
				<!--Start Pagination-->
@php
  $pagination = range(1,20)
@endphp
					<div class="pagination">
						<ul>
							 @foreach($pagination as $n => $page)
								@if( input()->get('page') == $n+1 )
									<li class="current"><span class="currenttext">{{ $page }}</span></li>
								@elseif( $page == end($pagination) )
									<li><a class="inactive" href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a></li>
								@else
									<li><a class="inactive" href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a></li>
								@endif
							@endforeach
						</ul>
					</div>

@endsection
