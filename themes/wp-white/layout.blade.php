<!DOCTYPE html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
   <head>
      @include('header')
      <link rel="stylesheet" href="{{ theme_url('css/style.css') }}" type="text/css" />      
      <style id='stylesheet-inline-css' type='text/css'>
         input#author:focus, input#email:focus, input#url:focus, #commentform textarea:focus, .widget .wpt_widget_content #tags-tab-content ul li a { border-color:#7b011b;}
         .widget h3, .frontTitle, .footer-navigation ul, .article, .description_images_bottom, .footer-navigation a, .sidebarpostviews,.dashed,.dashedpost,.dashedattach, .dashedsocial, .total-comments, #respond h4 { border-color:#7b011b;}
         #logo a, a:hover, .menu .current-menu-item > a, .menu .current-menu-item, .current-menu-ancestor > a.sf-with-ul, .current-menu-ancestor, footer .textwidget a, .single_post a, #commentform a, .copyrights a:hover, a, footer .widget li a:hover, .menu > li:hover > a, .single_post .post-info a, .post-info a, .readMore a, .reply a, .fn a, .carousel a:hover, .single_post .related-posts a:hover, .textwidget a, footer .textwidget a, .sidebar.walleft1 a:hover, .sidebar.walleft2 a:hover, .smallbutton a:hover, .related-posts2 li a:hover, .featured-cat,.featured-cat-result,.infoviews { color:#7b011b; }
         .nav-previous a, .nav-next a, .header-button, .sub-menu, #commentform input#submit, .tagcloud a:hover, #tabber ul.tabs li a.selected, .wall-subscribe input[type='submit'],.pagination a, .widget .wpt_widget_content #tags-tab-content ul li a, .latestPost-review-wrapper, .sidebarmenunavigation a { background-color:#7b011b; color: #fff; }
      </style>
      <link rel="stylesheet" id="responsive-css"  href="{{ theme_url('css/responsive.css') }}" type="text/css" media="all" />
      <link rel="stylesheet" id="white-css"  href="{{ theme_url('css/white.css') }}" type="text/css" media="all" />
   </head>
   <body id ="blog" class="post-template-default single single-post postid-353 single-format-standard main">
      <div class="main-container">
      <div id="page" class="single">
         <div class="content">

			<div class="article">
				@yield('content')
			</div>

			@include('sidebar')

			</div><!--.content-->
         </div>
         <!--#page-->
         <footer>
         </footer>
         <!--footer-->
         <div class="copyrights">
            <!--footer code-->
            <!--start copyrights-->
            <div class="footer-navigation">
               <ul id="menu-bottom" class="">

                  <li><a rel="follow" href="{{ home_url() }}">Home</a></li>
					@foreach( config('themes.page') as $page )
						<li><a rel="nofollow"href="{{ permalink($page, 'page') }}">{{ ucwords($page) }}</a></li>
					@endforeach
                  <li><a href="{{ home_url('sitemap-image.xml') }}">Sitemap</a></li>


               </ul>
            </div>
            <div class="row" id="copyright-note">
               <div class="copyright-left-text"> Theme by <a href="{{ home_url() }}"> {{ sitename() }}</a></div>
               <div class="copyright-text">Copyright &copy; 2017 <a href="{{ home_url() }}" title="White Wallpaper Wordpress Themes" rel="nofollow">WP White Themes</a> - White Wallpaper Wordpress Themes</div>
            </div>
            <!--end copyrights-->
         </div>
         <div id="su-footer-links" style="text-align: center;"></div>
      </div>
      <!--.main-container-->
	  @include('footer')
   </body>
</html>
