<!-- End Sidebar -->
<aside class="sidebar walleft2">
<div id="sidebars" class="sidebar">
<div class="sidebar_list">
<li id="search-2" class="widget widget-sidebar widget_search"><form method="post" id="searchform" class="search-form" action="{{ home_url() }}" _lpchecked="1">
<fieldset>
<input type="text" name="s" id="s" value="Search the site" onblur="if (this.value == '') {this.value = 'Search the site';}" onfocus="if (this.value == 'Search the site') {this.value = '';}" >
<input id="search-image" class="sbutton" type="submit" style="border:0; vertical-align: top;" value="Search">
</fieldset>
</form>
	</li>
	</div>
<div class="sidebarpostviews"<h3>Advertisement</h3></div>
<div class="sidebarads160">
	@yield('ads')
</div>
</div>
</aside>

@if(isset($random_terms))
<!-- Start Sidebar -->
<aside class="sidebar walleft2">
<div id="sidebars" class="sidebar">
<div class="sidebar_list">

<li id="wall_recent_posts_widget-2" class="widget widget-sidebar widget_wall_recent_posts_widget">
	<h3>Recent Posts</h3>
	<ul class="advanced-recent-posts">
		@foreach( array_slice( $random_terms, 0, 100) as $terms ) 
		  <li>
			  <a href="{{ permalink($terms) }}" title="{{ $terms }}" rel="bookmark">{{ ucwords($terms) }}</a>
		  </li>
		@endforeach
	</ul>
</li>
</div>
</div>
</aside>
@endif
