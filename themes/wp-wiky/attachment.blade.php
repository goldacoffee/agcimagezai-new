@extends('layout')

@section('content')

<div class="col-md-12">
	<h1>{{ $query }}</h1>
</div>

<div class="col-md-8">
				<div class="ads">
{!! ads('responsive') !!}
</div>

<div class="bigimage">
<img src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" alt="{{ strtolower($query) }} {{ $gallery['current']['title'] }}" title="{{ strtolower($query) }} {{ $gallery['current']['title'] }}" />

	<h3>{{ $subquery }}</h3>
	<div class="ads">
			{!! ads('responsive') !!}
	</div>
</div>

<h3>Gallery of {{ $subquery }}</h3>

<div class="row masonry-container">

@foreach($results as $key => $item)
		<div class="col-sm-6 col-md-4 data">
				<a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">
						<img title="{{ strtolower($query) }} {{ $item['title'] }}" class="image_thumb" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" alt="{{ strtolower($query) }} {{ $item['title'] }}" />
				</a>
		<div class="cap">
				<h2>{{ strtolower($query) }} {{ $item['title'] }}</h2>
		</div>
		</div>
@endforeach

</div>

</div>

@endsection
