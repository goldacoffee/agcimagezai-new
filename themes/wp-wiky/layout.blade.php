<!DOCTYPE html>
<html>
<head>
	@include('header')
	@include('css')
</head>
<body>
<nav class="navbar navbar-default">
<div class="container">
	<div class="navbar-header">
		<a class="navbar-brand" href="/">{{ sitename() }}</a>
	</div>
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-right">
			@foreach( config('themes.page') as $page )
				<li><a rel="nofollow"href="{{ permalink($page, 'page') }}">{{ ucwords($page) }}</a></li>
			@endforeach
			<li>
				<a href="{{ home_url('sitemap.xml') }}">Sitemap</a>
			</li>
		</ul>
	</div>
</div>
</nav>
<div class="container">

@include('breadcrumb')

	<div class="contentindex">

@yield('content')

@if( !is_page() )
		<div class="col-md-4">
			<p class="panel-title">Random Post</p>
			<div class="ads_kiri">
					{!! ads('responsive') !!}
			</div>
			<ul>
					@foreach (array_slice($random_terms,0,100) as $term)
						<li class="sidekanan">
							<h3 class="h3sideindex"><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></h3>
						</li>
					@endforeach
			</ul>

			<div class="ads_kiri">
				{!! ads('responsive') !!}
			</div>

		</div>

@endif
		<div class="footer">
			<div class="col-md-12">
				<ul></ul>
			</div>
			Copyright &copy; {{ date('Y') }} All Right Reserved.
		</div>
	</div>
</div>

@include('footer')

</body>
</html>
