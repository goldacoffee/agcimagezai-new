@extends('layout')

@section('content')

@include('breadcrumb')
<?php
//$results = str_replace('https://' , 'https://i0.wp.com', '$results');
?>

<div class="konten">

<div class="big_images">
<h1>{{ $query }}</h1>
<noscript><span>ads/responsive.txt</span></noscript>
<br>
	
<a href="#" title="{{ $results[0]['title'] }} - {{ $query }}">
<img class="main-image" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/',$results[0]['small']) }}" data-src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/',$results[0]['url']) }}" title="{{ $results[0]['title'] }} - {{ $query }}" alt="{{ $results[0]['title'] }} - {{ $query }}">
<noscript>&lt;img src="{{ $results[0]['url'] }}" alt="{{ $results[0]['title'] }} - {{ $query }}" title="{{ $results[0]['title'] }} - {{ $query }}"/&gt;</noscript>
</a>

<h2>{{ $results[0]['title'] }}</h2>
</div>
<noscript><span>ads/link.txt</span></noscript>


<p>
@foreach($results as $key => $item)
@if($key < 8)
<p>
	{{ random_strings( array_slice( $results, $key, $key+5 ), 5, ' ', 'ucwords' ) }}
	</p>
@endif
@endforeach
</p>
<noscript><span>ads/responsive.txt</span></noscript>
@foreach($results as $key => $item)
@if($key < 6)
<div class="images">
  <a href="#" title="{{ strtolower($query) }} {{ $item['title'] }}">
    <img class="image-item" src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/', $item['small']) }}" data-src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/', $item['url']) }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
	<noscript>&lt;img src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/',$item['url']) }}" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}"/&gt;</noscript>
  </a>
  <h2><a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">{{ strtolower($query) }} - {{ $item['title'] }}</a></h2>

</div>

@endif
@endforeach

<div class="clear"></div>

<div class="related">
<h3>Related Post for {{ $query }}</h3>
<ul>
@foreach ($random_terms as $index => $term)

	@if($index < 5)
		<li><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></li>
	@endif

@endforeach
</ul>
</div>

</div>

@endsection
