@extends('layout')

@section('content')

@include('breadcrumb')

<h1>{{ $page_title }}</h1>

<br/>
<div class="konten">
@include('page/' . strtolower($page_title) )
</div>
@endsection
