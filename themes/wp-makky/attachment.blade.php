@extends('layout')

@section('content')

@include('breadcrumb')


<div class="konten">

<div class="big_images">
<h1>{{ $subquery }}</h1>


<br>
	
<a href="#" title="{{ $gallery['current']['title'] }} - {{ $query }}">
<img class="main-image" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" title="{{ $gallery['current']['title'] }} - {{ $query }}" alt="{{ $gallery['current']['title'] }} - {{ $query }}">
<noscript>&lt;img src="{{ $gallery['current']['url'] }}" alt="{{ $gallery['current']['title'] }} - {{ $query }}" title="{{ $gallery['current']['title'] }} - {{ $query }}"/&gt;</noscript>
</a>

<h2>{{ $gallery['current']['title'] }}</h2>
</div>
<noscript><span>ads/responsive.txt</span></noscript>

<p>
@foreach($results as $key => $item)
@if($key < 4)
<p>
	{{ random_strings( array_slice( $results, $key, $key+5 ), 5, ' ', 'ucwords' ) }}
	</p>
@endif
@endforeach
</p>

@foreach($results as $key => $item)
@if($key < 6)
<div class="images">
  <a href="#" title="{{ strtolower($query) }} {{ $item['title'] }}">
    <img class="image-item" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
	<noscript>&lt;img src="{{ $item['url'] }}" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}"/&gt;</noscript>
  </a>
  <h2><a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">{{ strtolower($query) }} - {{ $item['title'] }}</a></h2>
  <a target="_blank" href="{{ $item['url'] }}" title="{{ strtolower($query) }} {{ $item['title'] }}">.</a>
</div>

@endif
@endforeach
<noscript><span>ads/link.txt</span></noscript>
<div class="clear"></div>

<div class="related">
<h3>Related Post for {{ $subquery }}</h3>
<ul>
@foreach ($random_terms as $index => $term)

	@if($index < 5)
		<li><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></li>
	@endif

@endforeach
</ul>
</div>

</div>

@endsection
