
@extends('layout')

@section('content')

<h1 class="text-center">{{ sitename() }} - {{ config('site.description') }}</h1>

<div class="list-group">
<noscript><span>ads/responsive.txt</span></noscript>
@foreach($results as $key => $item)
@if($key < 12)
<div class="images">
  <a href="#" title="{{ strtolower($query) }} {{ $item['title'] }}">
    <img class="image-item" src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/',$item['small']) }}" data-src="{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/',$item['url']) }}" onerror="this.onerror=null;this.src='{{ str_replace(array('https://' , 'http://') , 'https://i0.wp.com/', $item['small']) }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
	<noscript>&lt;img src="{{ $item['url'] }}" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}"/&gt;</noscript>
  </a>
  <h2><a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">{{ strtolower($query) }} - {{ $item['title'] }}</a></h2>
  
</div>

@endif
@endforeach
<noscript><span>ads/link.txt</span></noscript>
<br/>
</div>


@endsection