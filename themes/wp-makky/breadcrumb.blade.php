<div class="breadcrumbs">
	<div class="bc" xmlns:v="http://rdf.data-vocabulary.org/#">
		<span typeof="v:Breadcrumb"><a class="l" href="{{ home_url() }}" rel="v:url" property="v:title">Home</a></span> &raquo;
		@if( is_home() )
				<span typeof="v:Breadcrumb">{{ config('site.description') }}</span>
		@elseif( is_page() )
			<span typeof="v:Breadcrumb"><a class="l" href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $page_title }}</a></span> &raquo;
			<span typeof="v:Breadcrumb">{{ $page_title }}</span>
		@elseif( is_search() )
			<span typeof="v:Breadcrumb"><a class="l" href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $query }}</a></span> &raquo;
			<span typeof="v:Breadcrumb">{{ $query }}</span>
		@elseif( is_attachment() )
			<span typeof="v:Breadcrumb"><a class="l" href="{{ permalink($query) }}" rel="v:url" property="v:title">{{ $query }}</a></span> &raquo;
			<span typeof="v:Breadcrumb"><a class="l" href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ $subquery }}</a></span> &raquo;
			<span typeof="v:Breadcrumb">{{ $subquery }}</span>
		@endif
	</div>
</div>
