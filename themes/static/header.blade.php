<script>
		var beforeload = (new Date()).getTime();
</script>

<meta charset="utf-8">
@if( !config('site.custom_meta') )
	@include('meta')
@else
	@include('metacustom')
@endif
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="google-site-verification" content="{{ meta_data('google') }}" />
<meta name="msvalidate.01" content="{{ meta_data('bing') }}" />

<link href="{{ home_url('feed') }}" rel="alternate" type="application/rss+xml" title="{{ sitename() }} RSS Feed" />
<link href="{{ get_permalink() }}" rel="canonical">
<meta itemprop="url" content="{{ get_permalink() }}" />

<link href="{{ assets_url('icon.png') }}" rel="shortcut icon" type="image/x-icon"/>
<link href="{{ assets_url('icon.png') }}" rel="apple-touch-icon"/>
<noscript><span>ads/auto.txt</span></noscript>