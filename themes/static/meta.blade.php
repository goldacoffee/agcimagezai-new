<!-- meta seo -->
@if( is_home() )

	<title>{{ config('site.title') }} - {{ config('site.description') }}</title>
	<meta name="description" content="{{ config('site.title') }} - {{ config('site.description') }}"/>

@elseif( is_search() )

	<title>[ {{ strtolower($query) }} ] | {{ random_strings($results, 10, ', ', 'strtolower', $shuffle = false ) }}</title>
	<meta name="description" content="[ {{ $query }} ] - {{ random_strings($results, 10, ', ', 'strtolower',  $shuffle = false ) }}"/>

@elseif( is_attachment() )

	<title>Gallery of {{ $subquery }} ~ {{ $query }}</title>
	<meta name="description" content="{{ $subquery }} ~ {{ $query }}"/>

@elseif( is_page() )

	<title>Page : {{ $page_title }} | {{ config('site.title') }}</title>
	<meta name="description" content="Page : {{ $page_title }} | {{ config('site.title') }}"/>

@elseif( is_list() )

	<title>{{ $list_title }} | {{ config('site.title') }}</title>
	<meta name="description" content="{{ $list_title }} | {{ config('site.title') }}"/>

@endif
	
<!-- meta robots -->
{!! meta_index($path) !!}
