<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>

	@include('header')

      <style media="screen"> body{font:14px sans-serif;color:#000;background:#FFF}.c:after,.c:before{display:table;line-height:0;content:"";clear:both}nav{background-color:#00BFFF;z-index:1000;top:0;position:fixed;margin-left:0;width:100%;left:0}@media screen and (max-width:309px){.innav{width:300px}#word{padding-top:140px}}@media screen and (min-width:310px){.innav{width:300px}#word{padding-top:140px}}@media screen and (min-width:610px){.innav{width:600px}#word{padding-top:90px}}@media screen and (min-width:910px){.innav{width:900px}#word{padding-top:70px}}@media screen and (min-width:1210px){.innav{width:1200px}#word{padding-top:30px}}.innav{margin:0 auto}#word ul,nav ul{list-style:none;margin:0;padding:0}#word li,nav li{position:relative;float:left}nav a{font-size:11px;color:#346690;font-family:sans-serif;position:relative;z-index:1;display:block;padding:13px;text-decoration:none;font-weight:700}nav a:hover{text-decoration:underline;background-color:rgba(0,0,0,.1)}.listhome li,.pagelink{background-color:#87CEEB;text-align:center}#word{padding-bottom:30px}.listimg{width:294px;height:300px;margin:0 3px 18px;float:left;overflow:hidden}.listimg p{margin:0;line-height:21px}.listhome li{overflow:hidden;width:270px;height:15px;margin:5px;padding:10px;-webkit-box-shadow:0 0 5px 0 rgba(0,0,0,.5);box-shadow:0 0 5px 0 rgba(0,0,0,.5)}.listhome li a{line-height:21px;text-decoration:none;color:#346690}.listhome li a:hover{text-decoration:underline}.pagelink{padding:20px 0;-webkit-box-shadow:0 0 5px 0 rgba(0,0,0,.5);box-shadow:0 0 5px 0 rgba(0,0,0,.5)}.btm{background-color:#000;color:#FFF;padding:1px 0;text-align:center}.ads{float:left;margin-bottom:10px;width:100%} </style>
   </head>
   <body>
      <nav>
         <div class="innav">
            <ul class="c">
               <li><a href="/">HOME</a></li>

			   @foreach( range('A', 'Z') as $list)
				     <li><a href="{{ permalink($list, 'list_term') }}" rel="bookmark">{{ $list }}</a></li>
			   @endforeach
            </ul>
         </div>
      </nav>
      <section id="word">
         <div class="innav">

			@yield('content')

         </div>
      </section>
      <footer>
         <div class="innav pagelink">
            <p>
			@foreach( config('themes.page') as $page )
				<a href="{{ permalink($page, 'page') }}">{{ ucwords($page) }}</a> |
			@endforeach
				<a href="{{ home_url('sitemap.xml') }}">Sitemap</a>
			</p>
         </div>
         <div class="innav btm">
            <p>Copyright &copy; {{ date('Y') }}. Some Rights Reserved.</p>
         </div>
      </footer>
@include('footer')
   </body>
</html>
