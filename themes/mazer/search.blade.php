@extends('layout')

@section('content')

  <h1>{{ $query }} - {{ ucwords($results[0]['title']) }}</h1>
  <ul class="c listsingle">

    <div class="ads">
      {!! ads('responsive') !!}
    </div>


    @for ($i = 0; $i <= 7; $i++)




      <div class="listimg">
        <a href="{{ attachment_url($query, $results[$i]['title']) }}" class="th"
          title="{{ strtolower($query) }} {{ $results[$i]['title'] }}">
          <img width="294" height="230" title="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}"
            alt="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}" src="{{ $results[$i]['thumbnail'] }}"
            data-src="{{ $results[$i]['thumbnail'] }}"
            onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';" />
        </a>

        <p>{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}
          <a target="_blank" href="{{ $results[$i]['url'] }}">.</a>
        </p>
      </div>

    @endfor

    <div class="ads">
      {!! ads('responsive') !!}
    </div>

    @for ($i = 8; $i <= count($results) - 1; $i++)
      <div class="listimg">
        <a href="#">
          <img width="294" height="230" title="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}"
            alt="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}" src="{{ $results[$i]['thumbnail'] }}"
            data-src="{{ $results[$i]['url'] }}"
            onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';" />
        </a>
        <p>{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}
          <a target="_blank" href="{{ $results[$i]['url'] }}">.</a>
        </p>
      </div>
    @endfor

  </ul>

  <ul class="c listhome">
    @foreach ($random_terms as $term)
      <li><a href="{{ permalink($term) }}">{{ $term }}</a></li>
    @endforeach
  </ul>

@endsection
