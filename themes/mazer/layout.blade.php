<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    /* starts Material Icons CSS */
    /* in case of update Compare with https://fonts.googleapis.com/icon?family=Material+Icons */
    /* fallback */
    @font-face {
      font-family: 'Material Icons';
      font-style: normal;
      font-weight: 400;
      src: url(https://fonts.gstatic.com/s/materialicons/v98/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2');
      font-display: block;
    }

    .material-icons {
      font-family: 'Material Icons';
      font-weight: normal;
      font-style: normal;
      font-size: 24px;
      line-height: 1;
      letter-spacing: normal;
      text-transform: none;
      display: inline-block;
      white-space: nowrap;
      word-wrap: normal;
      direction: ltr;
      -webkit-font-feature-settings: 'liga';
      -webkit-font-smoothing: antialiased;
    }

    /* Ends Material Icons CSS */

  </style>
  <link rel="stylesheet" href="/assets/mazer/css/style.css">
  </style>
  <script defer src="/assets/mazer/js/script.js"></script>
</head> <input type="hidden" id="viewName" value="index"> <input id="fTitle" type="hidden"
  value="Free Online Tools For Developers - to Beautify, Validate, Minify, Analyse, Convert JSON, XML, JavaScript, CSS, HTML, Excel" />

<body>
  <div id="app">
    <section class="hero">
      <div class="hero-head">
        <nav class="navbar is-transparent" role="navigation" aria-label="main navigation">
          <div class="container is-fluid">
            <div class="navbar-brand">
              <a class="navbar-item" href="/">
                <img src="/img/slogo.png" alt="CodeBeautify Logo" aria-label="Logo" srcset="" width="217" height="44">
              </a>
              <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
                data-target="navbarTopMain">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
              </a>
            </div>
            <div class="navbar-menu" id="navbarTopMain">
              <div class="navbar-end">
                @foreach (range('A', 'Z') as $list)
                  <a href="{{ permalink($list, 'list_term') }}" rel="bookmark"
                    class="navbar-item is-link">{{ $list }}</a>
                @endforeach

              </div>
            </div>
          </div>
        </nav>
      </div>
    </section>

    @yield('content')


    </section>





    <section class="section has-background-light is-small ">
      <!-- <div class="container is-fluid">
        <div class="columns">
          <div class="column is-4">
            <div class="container">
              <figure class="image is-64x64 has-text-info"> <i class="material-icons md-72">school</i> </figure> <br>
              <h3 class="title is-4 is-spaced">What is CodeBeautify ?</h3>
              <p class="subtitle is-5"> CodeBeautify is an online code beautifier that allows you to beautify your
                source code. CodeBeautify also provides lots of tools that help to save developer's time. Use search to
                find more tools. </p>
            </div>
          </div>
          <div class="column is-4">
            <div class="container">
              <figure class="image is-64x64 has-text-info"> <i class="material-icons md-72">help</i> </figure> <br>
              <h3 class="title is-4 is-spaced">How to use CodeBeautify ?</h3>
              <p class="subtitle is-5"> Choose your Programming language, enter the source code ... and you are ready to
                go! </p>
            </div>
          </div>
          <div class="column is-2">
            <div class="container"> <span class="image is-64x64 has-text-info"> <i
                  class="material-icons md-72">person_add_alt_1</i> </span> <br>
              <h3 class="title is-4 is-spaced">Follow us on . .</h3>
              <div> <a class="icon has-text-dark ml-2" target="_blank" href="https://www.facebook.com/codebeautify"
                  title="Facebook" aria-label="Facebook" rel="noreferrer"> <i class="material-icons md-36">facebook</i>
                </a> <a class="ml-3" target="_blank" href="https://twitter.com/Codebeautify" aria-label="Twitter"
                  rel="noreferrer"> <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#363636"
                    class="bi bi-twitter" viewBox="0 0 16 16">
                    <path
                      d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" />
                  </svg> </a> </div>
            </div>
          </div>
          <div class="column is-2">
            <div class="container"> <span class="image is-64x64 has-text-info"> <i
                  class="material-icons md-72">share</i> </span> <br>
              <h3 class="title is-4 is-spaced">Share on social media</h3>
              <div> <a class="ml-2" target="_blank"
                  href="https://twitter.com/share?url=https%3A%2F%2Fcodebeautify.org&text=Amazing%20Collection%20of%20Tools%20for%20Developers"
                  aria-label="Twitter" rel="noreferrer"> <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                    fill="#363636" class="bi bi-twitter" viewBox="0 0 16 16">
                    <path
                      d="M5.026 15c6.038 0 9.341-5.003 9.341-9.334 0-.14 0-.282-.006-.422A6.685 6.685 0 0 0 16 3.542a6.658 6.658 0 0 1-1.889.518 3.301 3.301 0 0 0 1.447-1.817 6.533 6.533 0 0 1-2.087.793A3.286 3.286 0 0 0 7.875 6.03a9.325 9.325 0 0 1-6.767-3.429 3.289 3.289 0 0 0 1.018 4.382A3.323 3.323 0 0 1 .64 6.575v.045a3.288 3.288 0 0 0 2.632 3.218 3.203 3.203 0 0 1-.865.115 3.23 3.23 0 0 1-.614-.057 3.283 3.283 0 0 0 3.067 2.277A6.588 6.588 0 0 1 .78 13.58a6.32 6.32 0 0 1-.78-.045A9.344 9.344 0 0 0 5.026 15z" />
                  </svg> </a> <a class="ml-3" target="_blank"
                  href="https://www.linkedin.com/sharing/share-offsite/?url=https://codebeautify.org"
                  aria-label="LinkedIn" rel="noreferrer"> <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                    fill="#363636" viewBox="0 0 16 16">
                    <path
                      d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                  </svg> </a> <a class="icon has-text-dark ml-2" target="_blank"
                  href="https://www.facebook.com/sharer.php?s=100&p[url]=https%3A%2F%2Fcodebeautify.org"
                  title="Facebook" aria-label="Facebook" rel="noreferrer"> <i class="material-icons md-36">facebook</i>
                </a> </div>
            </div>
          </div>
        </div>
      </div> -->
    </section>
    <footer id="jumptofooter" class="footer">
      <!-- <div class="container is-fluid">
        <section class="has-text-centered">
          <h2 class="subtitle is-size-2 "> <strong>Code Beautify</strong> </h2>
        </section>
        <hr>
        <div class="columns has-text-centered-mobile">
          <div class="column">
            <p class="title has-text-dark is-5">Color Converters</p>
            <ul class="content">
              <li><a class="is-size-4-mobile" href="/hex-to-pantone-converter">HEX to Pantone Converter</a></li>
            </ul>
            <p class="title is-5"><a class="has-text-dark" href="/unit-tools">Unit Converter</a></p>

          </div>
        </div>
      </div> -->
    </footer>
    <section class="hero is-light">
      <div class="hero-body">
        <div class="container is-fluid">
          <div class="columns is-centered buttons are-medium"> <a class="button is-light" href="https://goo.gl/1oWmPe"
              target="_blank" title="Buy us a Coffee" rel="noopener">Buy us a Coffee</a> <a class="button is-light"
              href="https://jsonformatter.org" target="_blank" title="JSON Formatter" rel="noopener">JSON Formatter</a>
            <a class="button is-light" href="/faq">FAQ</a> <a class="button is-light" href="/policy">Privacy Policy</a>
            <a class="button is-light" href="/aboutus">About</a> <a class="button is-light"
              href="/contactus">Contact</a> <a class="button is-light" href="/history">History</a> <a
              class="button is-light" href="/where-am-i-right-now">Where am I right now?</a> <a class="button is-light"
              href="https://codeblogmoney.com" rel="noopener">Blog</a> <a class="button is-light"
              href="/calculators/">Calculators</a>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer has-background-dark" style="padding-bottom: 1rem;">
      <div class="container has-text-centered">
        <p class="has-text-light"> <span class="has-text-light has-text-weight-bold">Copyright ©</span> CodeBeautify
          2021 | v4.7 </p> <br>
        <p class="has-text-light is-size-4"> Built with <span class="icon has-text-primary"><i
              class="material-icons md-18">local_cafe</i></span> and <span class="icon has-text-danger"><i
              class="material-icons md-18">favorite</i></span> for you... </p>
      </div>
    </footer>
  </div>


  <div id="errorModal" class="modal">
    <div class="modal-background" onclick="closeAllModal();"></div>
    <div class="modal-content">
      <article class="message is-danger">
        <div class="message-header">
          <p>Error Message</p>
        </div>
        <div class="message-body"> <strong>
            <p id="errorMessage"></p>
          </strong>
          <div class="has-text-right"> <button class="button is-danger" onclick="closeAllModal();">Close</button> </div>
        </div>
      </article>
    </div> <button class="modal-close is-large" aria-label="close" onclick="closeAllModal();"></button>
  </div>
</body>

</html>
