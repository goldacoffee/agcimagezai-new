<?php

namespace Masterzain\Agc;

use Masterzain\Classes\FastCache;
use Masterzain\Classes\Grabbing;

class Wallpaper
{

	protected $_longTitle 	= false;
	protected $_page;
	protected $_dataSource;

	protected function getDataSource($source = "google")
	{
		$GoogleParams = [
			'async' 	=>  '_id:rg_s,_pms:s',
			'ei' 			=>  'HrjEWOqhCcWZ8gWh16oI',
			'yv' 			=>  '2',
			'start'		=> 	'0',
			'asearch' =>  'ichunk',
			'tbm' 		=>  'isch',
			'vet' 		=>  '10ahUKEwjqqKiT-8_SAhXFjLwKHaGrCgEQuT0IGCgB.HrjEWOqhCcWZ8gWh16oI.i',
			'ved' 		=>  '0ahUKEwjqqKiT-8_SAhXFjLwKHaGrCgEQuT0IGCgB',
			'ijn' 		=> 	'0',
			'safe'    =>  'active',
			'dcr'     =>  '0',
			'q' 			=> 	'',
		];
		$endpoint = [
			// 'google'  => 'http://google.com/search?' . http_build_query($GoogleParams), // old
			'google' => "https://www.google.com/search?q=",
			'bing'    => 'http://www.bing.com/images/search?FORM=IBASEP&count=28&first=1&q=',
			'yahoo'	  => 'http://images.search.yahoo.com/search/images?ei=UTF-8&fr=sfp&save=1&p=',
			'yandex'  => 'https://yandex.com/images/search?text=',
		];
		return $endpoint[$source];
	}

	public function longTitle($value = "")
	{
		$this->_longTitle = $value;
		return $this;
	}

	public function setPage($value = "")
	{
		$this->_page = $value;
		return $this;
	}

	public function dataSource($source = "")
	{
		$this->_dataSource = $source;
		return $this;
	}

	public function getExternalData($keyword, $page = "")
	{
		$url = config('extra.external_grabber_url') . config('agc.default_search') . '/' . $keyword . '?api_key=' . config('extra.external_grabber_api_key');
		$uas = [
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.68",
			"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0",
			"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Vivaldi/3.6",
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Vivaldi/3.6",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11.2; rv:85.0) Gecko/20100101 Firefox/85.0",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Vivaldi/3.6",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.63",
		];

		// $proxy = [];

		$ua = $uas[array_rand($uas)];
		// if (!empty($proxy)) {
		// 	$proxy = "tcp://$proxy";
		// }

		$options = [
			"http" => [
				"method" => "GET",
				// "proxy" => "$proxy",
				"user_agent" => $ua,
			],
			"ssl" => [
				"verify_peer" => false,
				"verify_peer_name" => false,
			],
		];
		$context = stream_context_create($options);
		$response = file_get_contents($url, false, $context);

		$data = json_decode($response,true);



		return $data;
	}

	public function find($keyword, $page = "0")
	{


		if (config('extra.external_grabber_status')) {

			return [
				"data" => $this->getExternalData($keyword, $page)
			];

			die;
		}

		$this->setPage($page);
		$this->dataSource(config('agc.default_search'));
		$defaultSource	 = $this->_dataSource;

		$sourceCache = getSource();





		$keyword = limit_the_words(clean($keyword), 8);
		$keyword = is_keyword($keyword);



		switch ($sourceCache) {
			case "bing";
				return [
					'keyword'		=> $keyword,
					'data'		=> $this->bing(str_replace(' ', '+', $keyword)),
				];
				break;
			case "yahoo";
				return [
					'keyword'		=> $keyword,
					'data'		=> $this->yahoo(str_replace(' ', '+', $keyword)),
				];
				break;
			case "yandex";
				return [
					'keyword'		=> $keyword,
					'data'		=> $this->yandex(str_replace(' ', '+', $keyword)),
				];
				break;
			default:

				return [
					'keyword'		=> $keyword,
					'data'			=> $this->google(str_replace(' ', '+', $keyword)),
				];
		}
	}

	private function google($keyword)
	{


		// $url = "https://www.google.com/search?q=" . urlencode($keyword) . "&source=lnms&tbm=isch&tbs=";
		$url = "https://www.google.com/search?q=" . urlencode($keyword) . "&source=lnms&tbm=isch&tbs=isz:l";
		$uas = [
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.68",
			"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0",
			"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Vivaldi/3.6",
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Vivaldi/3.6",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11.2; rv:85.0) Gecko/20100101 Firefox/85.0",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Vivaldi/3.6",
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.63",
		];

		// $proxy = [];

		$ua = $uas[array_rand($uas)];
		// if (!empty($proxy)) {
		// 	$proxy = "tcp://$proxy";
		// }

		$options = [
			"http" => [
				"method" => "GET",
				// "proxy" => "$proxy",
				"user_agent" => $ua,
			],
			"ssl" => [
				"verify_peer" => false,
				"verify_peer_name" => false,
			],
		];

		$context = stream_context_create($options);


		$response = file_get_contents($url, false, $context);



		$exploded = explode(
			"AF_initDataCallback({key: 'ds:1', isError:  false , hash: '4', data:",
			$response
		);

		$data = isset($exploded[1]) ? $exploded[1] : "";

		$data = explode(", sideChannel: {}});</script>", $data);
		$data = $data[0];

		$data = json_decode($data, true);



		$rawResults = [];
		$results = [];

		if (isset($data[31][0][12][2])) {
			$rawResults = $data[31][0][12][2];
		}


		foreach ($rawResults as $rawResult) {

			$result = [];


			$this->filterResult($rawResult, $result);

			$data = $this->getValues($result);


			$result = [];




			if (count($data) >= 11) {
				$result["keyword"] = $keyword;
				$result["slug"] = $this->slug($keyword);



				$result["title"] = isset($data[19])
					? ucwords(str_replace('-', ' ', $this->slug($data[19])))
					: "";
				$result["alt"] = isset($data[19])
					? str_replace('-', ' ', $this->slug($data[19]))
					: "";



				// $result["url"] = $data[8];
				$result["filetype"] = $this->getFileType($data[8]);
				$result["width"] = $data[6];
				$result["height"] = $data[7];
				$result["source"] = isset($data[12]) ? $data[12] : "";
				// $result["domain"] = isset($data[20]) ? $data[20] : "";
				$result["domain"] = isset($data[23]) ? $data[23] : "";

				$result["thumbnail"] = isset($data[26]) ? $data[26] : $data[1];
				$result['image'] = $data[8];
				$result['small'] = isset($data[26]) ? $data[26] : $data[1];
				// $result['copy'] 	= $arr['isu'];
				$result['size'] 	= $data[6] . $data[7];






				// var_dump($result);
				// die;



				if (strpos($result["url"], "http") !== false) {
					$results[] = $result;
				}

				$results[] = $result;
			}
		}


		return $results;



		/** 
		 * below this line is old code 
		 * so ignored
		 * 
		 * */
		if (config('proxy.active')) {
			$proxylist 		= get_one(config('proxy.proxy'));
			$proxy 				= (strpos($proxylist, '|')) ? explode('|', $proxylist)[0] : $proxylist;
			$account 			= (strpos($proxylist, '|')) ? explode('|', $proxylist)[1] : null;
		}

		$data_source  = [
			// 'url' 		    	=> $this->getDataSource('google') . $keyword,
			'url' 		    	=>  "https://www.google.com/search?q=" . urlencode($keyword) . "&source=lnms&tbm=isch&tbs=isz:l",
			'useragent' 		=> 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
			'referer' 	  	=> 'https://www.google.com/search?source=hp&q=' . urlencode($keyword) . '&oq=' . urlencode($keyword),
			'proxy'		    	=> (config('proxy.active')) ? $proxy : '',
			'account_proxy'	=> (config('proxy.active')) ? $account : '',
			'source'				=> 'google',
		];



		$html         = Grabbing::curl($data_source);
		$results 			= array();
		$resultsArray = json_decode($html, true);

		if (isset($resultsArray[1][1]) && !empty($resultsArray[1][1])) {
			preg_match_all('~rg_meta notranslate">\\K.*(?=</div>)~Uis', $resultsArray[1][1], $output);

			if (!empty($output[0])) {
				for ($i = 0; $i <= count($output[0]) - 1; $i++) {
					$arr 	= json_decode($output[0][$i], true);
					$content['image'] = $arr['ou'];
					$content['small'] = $arr['tu'];
					$content['title'] = (config('agc.filter_badwords_grab')) ? clean(filter_badstrings($arr['pt'])) : clean($arr['pt']);
					$content['copy'] 	= $arr['isu'];
					$content['size'] 	= $arr['ow'] . ' x ' . $arr['oh'] . ' px';
					$results[] 				= $content;
				}
			}
		}

		if (!empty($results)) {
			updateSource('google');
		}

		return (!empty($results)) ? $results : self::bing($keyword);
	}


	public  function slug($text, string $divider = '-')
	{


		$pecah = explode('/', $text);

		$text = end($pecah);
		// replace non letter or digits by divider
		$text = preg_replace('~[^\pL\d]+~u', $divider, $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, $divider);

		// remove duplicate divider
		$text = preg_replace('~-+~', $divider, $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	private function bing_execute($keyword)
	{
		if (config('proxy.active')) {
			$proxylist 		= get_one(config('proxy.proxy'));
			$proxy 				= (strpos($proxylist, '|')) ? explode('|', $proxylist)[0] : $proxylist;
			$account 			= (strpos($proxylist, '|')) ? explode('|', $proxylist)[1] : null;
		}

		$data_source  = array(
			'url' 			  	=> $this->getDataSource('bing') . $keyword,
			'useragent' 		=> 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
			'referer' 			=> '',
			'proxy'		    	=> (config('proxy.active')) ? $proxy : '',
			'account_proxy'	=> (config('proxy.active')) ? $account : '',
			'source'				=> 'bing',
		);

		$html         = Grabbing::curl($data_source);

		$grabbing 		= [
			'images'  => Grabbing::parse('<a class="thumb" target="_blank" href="', '"', $html),
			'titles'  => Grabbing::parse('<div class="des">', '</div>', $html),
			'small'   => Grabbing::parse('width="230" src="', '&amp', $html),
			'size'    => Grabbing::parse('class="fileInfo">', '</div>', $html),
			'copy'    => Grabbing::parse('<a class="tit" target="_blank" href="', '"', $html),
		];

		return $grabbing;
	}

	private function bing($keyword)
	{

		$grabbing = self::bing_execute($keyword);


		if (empty($grabbing['images'])) {
			$grabbing = self::bing_execute(limit_the_words($keyword, 2));
		}

		// if( ( $this->_longTitle ) ) {
		// 	$count_data = count($grabbing['titles']);
		// 	for ($i=0; $i<$count_data; $i++){
		// 	  $j = $i+1;
		// 	  if ($i==$count_data-1){
		// 		$data2 = $grabbing['titles'][0];
		// 	  }
		// 	  else{
		// 			$data2 = $grabbing['titles'][$j];
		// 	  }
		// 			$title[] = $grabbing['titles'][$i]." ".$data2;
		// 	}
		//
		// } else {
		// 	$title = $grabbing['titles'];
		// }

		foreach ($grabbing['images'] as $key => $val) {
			$output[$key] = array(
				'title' => (config('agc.filter_badwords_grab')) ? clean(filter_badstrings($grabbing['titles'][$key])) : clean($grabbing['titles'][$key]), //clean( $title[$key] ),
				'image' => $val,
				'small' => $grabbing['small'][$key],
				'size'  => $grabbing['size'][$key],
				'copy'  => $grabbing['copy'][$key],
			);
		}

		if (!empty($output)) {
			updateSource('bing');
		}

		if (!isset($output) || empty($output)) {
			redirection();
		}
		return $output;
	}

	private function yahoo($keyword)
	{
		if (config('proxy.active')) {
			$proxylist 		= get_one(config('proxy.proxy'));
			$proxy 			= (strpos($proxylist, '|')) ? explode('|', $proxylist)[0] : $proxylist;
			$account 		= (strpos($proxylist, '|')) ? explode('|', $proxylist)[1] : null;
		}

		$data_source  = array(
			'url' 			  	=> $this->getDataSource('yahoo') . $keyword,
			'useragent' 			=> 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
			'referer' 			=> '',
			'proxy'		    	=> (config('proxy.active')) ? $proxy : '',
			'account_proxy'		=> (config('proxy.active')) ? $account : '',
			'minify'				=> true,
			'source'				=> 'yahoo',
		);

		$html         = Grabbing::curl($data_source);

		preg_match_all('~<li class="ld " data=\'\K.*(?=\')~Uis', $html, $output);

		if (isset($output[0][0]) && !empty($output[0][0])) {
			$results = array();
			foreach ($output[0] as $key => $item) {
				$jsonData = json_decode($item, true);
				$results[] = array(
					'title' => (config('agc.filter_badwords_grab')) ? clean(filter_badstrings($jsonData['alt'])) : clean($jsonData['alt']),
					'image' => $jsonData['iurl'],
					'small' => $jsonData['ith'],
					'size'  => $jsonData['w'] . ' x ' . $jsonData['h'] . ' px',
					'copy'  => parse_url($jsonData['a'], PHP_URL_HOST),
				);
			}
		}
		if (!empty($results)) {
			updateSource('yahoo');
		}

		return (!empty($results)) ? $results : self::bing($keyword);
	}

	private function yandex($keyword)
	{
		if (config('proxy.active')) {
			$proxylist 		= get_one(config('proxy.proxy'));
			$proxy 			= (strpos($proxylist, '|')) ? explode('|', $proxylist)[0] : $proxylist;
			$account 		= (strpos($proxylist, '|')) ? explode('|', $proxylist)[1] : null;
		}

		$data_source  = array(
			'url' 			  	=> $this->getDataSource('yandex') . $keyword,
			'useragent' 			=> 'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
			'referer' 			=> '',
			'proxy'		    	=> (config('proxy.active')) ? $proxy : '',
			'account_proxy'		=> (config('proxy.active')) ? $account : '',
			'minify'				=> true,
			'source'				=> 'yandex',
		);

		$html         = Grabbing::curl($data_source);

		preg_match_all('~serp-item":\K.*(?=}\')~Uis', $html, $output);

		if (isset($output[0][0]) && !empty($output[0][0])) {
			$results = array();
			foreach ($output[0] as $key => $item) {
				$jsonData = json_decode($item, true);
				$results[] = array(
					'title' => (config('agc.filter_badwords_grab')) ? clean(filter_badstrings($jsonData['snippet']['title'])) : clean($jsonData['snippet']['title']),
					'image' => $jsonData['img_href'],
					'small' => $jsonData['thumb']['url'],
					'size'  => $jsonData['preview'][0]['width'] . ' x ' . $jsonData['preview'][0]['height'] . ' px',
					'copy'  => parse_url($jsonData['img_href'], PHP_URL_HOST),
				);
			}
		}

		if (!empty($results)) {
			updateSource('yandex');
		}

		return (!empty($results)) ? $results : self::bing($keyword);
	}

	public function random_key($md5_key = "")
	{


		$cache = new FastCache;



		$all_keys = $cache->get("list_key_wallpaper");


		if (!$all_keys) {

			$all_keys = $this->saveKey();
		}


		if ($md5_key) {
			if (!in_array($md5_key, $all_keys)) {
				$new_key = array_merge([$md5_key], $all_keys);
				$new_key = array_slice($new_key, 0, 50);
				$cache->update("list_key_wallpaper", $new_key, 24 * 3600 * 30);
				$get_key = $new_key;
			} else {
				$get_key = $all_keys;
			}
		} else {
			$get_key = $all_keys;
		}
		// echo $get_key;
		// die;
		return get_one($get_key);
	}

	protected function takeWhile($keyword)
	{

		$data = $this->find($keyword);


		if (empty($data)) {
			return $this->takeWhile(random_terms(1));
		}
		return [
			'keyword'	=> $keyword,
			'results'	=> $data,
		];
	}

	protected function saveKey()
	{
		$cache     = new FastCache;


		$takeWhile = $this->takeWhile(random_terms(1));

		// bugged

		$given_key = "new_key_" . md5($takeWhile['keyword']);
		if (isset($takeWhile['results']) && !empty($takeWhile['results'])) {
			// save the wallpaper
			$cache->save($given_key, $takeWhile['results']);
			// save the cache key of wallpaper data
			$cache->save("list_key_wallpaper", [$given_key], 24 * 3600 * 30);
		}
		return $cache->get("list_key_wallpaper");
	}


	// add
	public function filterResult($array, &$result)
	{
		$array = array_filter($array);

		foreach ($array as $key => $value) {
			$data = [];

			if (filter_var($value, FILTER_VALIDATE_URL)) {
				$result[] = array_filter(array_flatten($array));
			}

			if (is_string($value)) {
				$result[] = $value;
			}

			if (is_array($value)) {
				$this->filterResult($value, $result);
			}
		}
	}
	public  function array_flatten($array)
	{
		$return = [];
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				$return = array_merge($return, $this->array_flatten($value));
			} else {
				$return[$key] = $value;
			}
		}
	}


	public static function getValues($array)
	{
		$return = [];
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				foreach ($value as $vk => $vv) {
					$return[] = $vv;
				}
			} else {
				$return[] = $value;
			}
		}

		return $return;
	}
	public function getFileType($url)
	{
		$url = strtolower($url);

		switch ($url) {
			case strpos($url, ".jpg") || strpos($url, ".jpeg"):
				return "jpg";
				break;

			case strpos($url, ".png"):
				return "png";
				break;

			case strpos($url, ".bmp"):
				return "bmp";
				break;

			case strpos($url, ".gif"):
				return "gif";
				break;

			default:
				return "jpg";
				break;
		}
	}
}
