<?php

function kwsugest($keyword)
{
    $keyword = urlencode($keyword);
    $output  = array();
    $url   = 'http://api.bing.com/osjson.aspx?query=' . $keyword;
    $url2  = 'http://suggestqueries.google.com/complete/search?output=firefox&client=firefox&hl=en-US&q=' . $keyword;
    $data  = json_decode(file_get_contents_utf8($url), true);
    $data2 = json_decode(file_get_contents_utf8($url2), true);

    if (isset($data[1]) && !empty($data[1])) {
        if (isset($data2[1]) && !empty($data2[1])) {
            $output = array_unique(array_merge($data[1], $data2[1]));
        } else {
            $output   = $data[1];
        }
    }

    return $output;
}

function filterImage($url)
{
    if (strpos($url, 'blogspot.com') !== false) {
        return str_replace('http://', 'https://', $url);
    } else {
        return $url;
    }
}
