<?php

class ErrorHandler
{

    public function register()
    {
        set_error_handler(array($this, 'handleError'), E_ALL | E_STRICT);
        set_exception_handler(array($this, 'handleException'));
        if (php_sapi_name() !== 'cli') {
			ini_set('display_errors', 1);
            register_shutdown_function(array($this, 'handleShutdown'));
        }
    }
	
    public function handleError($errno, $errstr, $errfile, $errline, $errcontext)
    {
        if (!(error_reporting() & $errno)) {
            return true;
        }
        switch ($errno) {
            case E_ERROR:
                $errno_str = 'E_ERROR';
                break;
            case E_WARNING:
                $errno_str = 'E_WARNING';
                break;
            case E_PARSE:
                $errno_str = 'E_PARSE';
                break;
            case E_NOTICE:
                $errno_str = 'E_NOTICE';
                break;
            case E_CORE_ERROR:
                $errno_str = 'E_CORE_ERROR';
                break;
            case E_CORE_WARNING:
                $errno_str = 'E_CORE_WARNING';
                break;
            case E_COMPILE_ERROR:
                $errno_str = 'E_COMPILE_ERROR';
                break;
            case E_COMPILE_WARNING:
                $errno_str = 'E_COMPILE_WARNING ';
                break;
            case E_USER_ERROR  :
                $errno_str = 'E_USER_ERROR';
                break;
            case E_USER_WARNING:
                $errno_str = 'E_USER_WARNING';
                break;
            case E_USER_NOTICE:
                $errno_str = 'E_USER_NOTICE';
                break;
            case E_STRICT:
                $errno_str = 'E_STRICT';
                break;
            case E_DEPRECATED:
                $errno_str = 'E_DEPRECATED';
                break;
            case E_USER_DEPRECATED:
                $errno_str = 'E_USER_DEPRECATED';
                break;
            default:
                $errno_str = 'UNKNOWN';
        }
        $message 	= $errno_str . ' [' . $errno . '] ' . $errstr . " in {$errfile}:{$errline}";
        $Exception 	= new \Exception();
		$this->display(
			[ 
				'title' 	=> $errno_str,
				'type'		=> $errno,
				'message'	=> $errstr,
				'file'		=> $errfile,
				'line'		=> $errline,
			],
			$Exception->getTrace()
		);
    }
	
    public function handleShutdown()
    {
        $error = error_get_last();
        if ($error !== null && $error['type'] === E_ERROR) {
			$this->display(
				[ 
					'title' 	=> 'SHUTDOWN',
					'type'		=> 'E_ERROR',
					'message'	=> $error['message'],
					'file'		=> $error['file'],
					'line'		=> $error['line'],
				]
			);		
        }
    }
	
    public function handleException(\Exception $Exception)
    {
		$this->display(
			[ 
				'title' 	=> 'EXCEPTION',
				'type'		=> get_class($Exception),
				'message'	=> $Exception->getMessage(),
				'file'		=> $Exception->getFile(),
				'line'		=> $Exception->getLine(),
			],
			$Exception->getTrace()
		);
    }

	private function display( $error, $trace = null )
	{
		if( ob_get_length() > 0) {
			ob_end_clean();
		}
        print "<div>";
        print "<h3 style='color: rgb(190, 50, 50);'>Whoops! {$error['title']}</h3>";
        print "<table style='width: 1000px; display: inline-block;'>";
        print "<tr style='background-color:rgb(240,240,240);'><th>Type</th><td>{$error['type']}</td></tr>";		
        print "<tr style='background-color:rgb(240,240,240);'><th>Error</th><td>{$error['message']}</td></tr>";
        print "<tr style='background-color:rgb(230,230,230);color:green;'><th>File</th><td><strong>{$error['file']}</strong></td></tr>";
        print "<tr style='background-color:rgb(240,240,240);'><th>Line</th><td>{$error['line']}</td></tr>";
		if( $trace ) {
			print "<tr style='background-color:rgb(240,240,240);'><th>Trace</th><td><ol>";
			array_shift($trace);
			foreach( $trace as $i => $l )
			{
			  if( isset($l['class']) ) print "<li>in function <b><span style='color: rgb(190, 50, 50);'>{$l['class']}{$l['type']}{$l['function']}</span></b></li>";
			  if( !isset($l['class']) ) print "<li>in function <b><span style='color: rgb(190, 50, 50);'>{$l['function']}</span></b></li>";
			  if( isset($l['file']) ) print " in <b>{$l['file']}</b>";
			  if( isset($l['line']) ) print " on line <b>{$l['line']}</b>";
			}
			print "</ol></td></tr>";
		}
        print "</table></div>";
		file_put_contents( BASE_PATH . '/error_log.txt', implode('#', $error) . "\n", FILE_APPEND );
        exit();
	}
}