<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="{{ str_replace('http://', 'https://', sitemap_xsl('image')) }}"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
  @if ($sitemap)
    @foreach ($sitemap as $n => $item)
      @if (filter_var($item['url'], FILTER_VALIDATE_URL))
        <url>
          <loc>{{ permalink(limit_the_words($item['title'], 5)) }}</loc>
          <lastmod>{{ sitemap_date() }}</lastmod>
          <changefreq>always</changefreq>
          <priority>0.5</priority>
          <image:image>
            <image:loc>{{ $item['url'] }}</image:loc>
            <image:caption>{{ ucwords($item['title']) }}</image:caption>
            <image:geo_location>United States</image:geo_location>
            <image:title>{{ ucwords($item['title']) }}</image:title>
            <image:license>{{ permalink($item['title']) }}</image:license>
          </image:image>
        </url>
      @endif

    @endforeach
  @endif
</urlset>

