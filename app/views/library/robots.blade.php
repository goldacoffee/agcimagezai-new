User-agent: *

Disallow: /{{ config('cache.dir') }}/

Sitemap: {{ home_url('sitemap.xml') }}
Sitemap: {{ home_url('sitemap-image.xml') }}
Sitemap: {{ home_url('feed') }}
Sitemap: {{ home_url('feed/image') }}
Sitemap: {{ home_url('atom') }}
Sitemap: {{ home_url('rss') }}
@foreach( $sitemap as $item )
Sitemap: {{ home_url( 'sitemap-' . $item . '.xml' ) }}
@endforeach
