<?php

/*
 *	Site Title & Description
 */

return [

	'title'         => 'Shefalitayal',
	'url'         => 'Shefalitayal.com',

	'description'   => 'Best Design Ideas',

	// if false, meta title & description will be displayed from /themes/static/meta.blade.php
	'custom_meta'	=> true,

	'index'	=>  [
		'title'			=> '{title} - {description} | {sitename}',
		'description'	=> '{title} - {description} | {sitename}',
	],

	'search' =>  [
		'title'			=> '{query|ucwords} | {random|results|5|, |strtolower|false}',
		'description'	=> '{query|strtolower} - {count} images - {random|results|5|, |strtolower|true}',
	],
	'attachment' =>  [
		'title'			=> 'Gallery for {subquery|strtolower} - {query|strtolower} | {random|results|5|, |strtolower|true}',
		'description'	=> 'Gallery for {subquery|strtolower} - {query|strtolower} | {random|results|5|, |strtolower|true}',
	],
	'page' =>  [
		'title'			=> '{page_title} Page - {title}',
		'description'	=> '{page_title} Page - {title}',
	],
	'list' =>  [
		'title'			=> 'Index post with abjad : {list_title} - {title}',
		'description'	=> 'Index post with abjad : {list_title} - {title}',
	],

];
