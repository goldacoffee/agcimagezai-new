<?php

/*
 *	App Configuration
 */

return [
  
  'blade'        	=> [
      'admin'       => 'app/views/admin',
      'library'     => 'app/views/library',
      'themes'      => 'themes/{%active_theme%}',
	    'default'	=> 'app/views',
  ],

];
