<?php

/*
 *	Remote Ads & Badwords Configuration
 */

return [

	// REMOTE ADS

	'ads'	=> [

			'remote'			=> false,
			'source'			=> [
				'responsive'	=> 'http://yourdomain.com/responsive.txt',
				'300x250'			=> 'http://yourdomain.com/300x250.txt',
				'336x280'			=> 'http://yourdomain.com/336x280.txt',
				'728x90'			=> 'http://yourdomain.com/728x90.txt',
				'etc'					=> 'fill here...',
			],
	],

	// REMOTE BADWORDS

	'badwords' => [

			'remote'		=> false,
			'source'		=> [
				'badwords'				=> 'http://yourdomain.com/badwords.txt',
				'commonwords'			=> 'http://yourdomain.com/commonwords.txt',
			],

	],

];
