<?php

/*
 *	Pinger Configuration
 */

return [

	'library'	=> [
			'disabled'		=> false,
			'sitemap_index'	=> 97,
			'sitemap_main'	=> 9999,
	],

	'pinger'	=> [
			'active'       	=> false,
			'every'		    => [ 0, 10, 20, 30, 40, 50 ], // in these minutes trigerred by visitor's coming
	],

	'redirect'	=> [
			'www_to_nonwww'	=> true,
	],

	'bad_domain' => [ 'lights2you.com.au', 'offroadhoverboard.net', 'www.garageliving.com', 'garageliving.com' ], // prevent some domain to access your IP VPS

];
