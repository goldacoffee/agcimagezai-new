<?php

use Masterzain\Agc\Wallpaper;
use Masterzain\Classes\Grabbing;

class WallpaperController extends BaseController
{

    public $wallpaper = null;

    public function __construct()
    {
        parent::__construct();
        $this->wallpaper = new Wallpaper;
    }

    /** halaman utama atau index **/
    public function index()
    {

        $cache_key        = $this->wallpaper->random_key();
        $output           = $this->cache->get($cache_key);
        $data['results']  = $output['data'];
        $data['query']    = $output['keyword'];
        $data['random_terms'] = random_terms(500);
        $data['path']     = __FUNCTION__; // index



        echo $this->blade->theme('index', $data);
    }

    /** mengontrol $_POST ke halaman search **/
    public function indexPost()
    {
        $squery = input()->post('s');

        // redirection(permalink($squery));
    }

    /** halaman agc atau search **/
    public function search()
    {
        $query = (func_num_args() == 1) ? func_get_args()[0] : func_get_args()[1];

        $cache_key = "search_" . md5($query);
        $output = $this->cache->get($cache_key);


        if (!$output['data']) {
            $output = $this->wallpaper->find($query);
            if ($output['data']) {
                $this->wallpaper->random_key($cache_key);
                $this->cache->save($cache_key, $output, null, ['search']);
            }
        }
        /**
         * added by ndower
         * shuffle data to make it more random
         */



        $output['data'] = $this->cleanOutputData($output['data']);

        // print_r($output['data']);
        // die;

        $data['results']      = $output['data'];
        $data['random_terms'] = random_terms(30);
        $data['query']        = ucwords(clean($query));
        $data['path']         = __FUNCTION__; // search


        echo $this->blade->theme("search", $data);
    }

    /** halaman fake gallery / attachment **/
    function attachment()
    {
        $args     = func_get_args();

        $query    = (func_num_args() == 2) ? $args[0] : $args[1];
        $subquery = (func_num_args() == 2) ? $args[1] : $args[2];




        $cache_key  = "search_" . md5($query);

        $output     = $this->cache->get($cache_key);

        if (!$output['data']) {
            $output = $this->wallpaper->find($query);
            if ($output['data']) {
                $this->wallpaper->random_key($cache_key);
                $this->cache->save($cache_key, $output, null, ['search']);
            }
        }

        /**
         * added by ndower
         * shuffle data to make it more random
         */
        $output['data'] = $this->cleanOutputData($output['data']);





        $gallery              = getGallery($subquery, $output['data']);

        $data['gallery']      = $gallery;
        $data['results']      = $output['data'];
        $data['query']        = ucwords(clean($query));
        $data['subquery']     = ucwords(clean($subquery));
        $data['random_terms'] = random_terms(30);
        $data['path']         = __FUNCTION__; // attachment





        echo $this->blade->theme("attachment", $data);
    }

    public function cleanOutputData($data)
    {
        $cleaned = [];

        foreach ($data as $item) {
            if (filter_var($item['url'], FILTER_VALIDATE_URL) && filter_var($item['domain'], FILTER_VALIDATE_DOMAIN)) {
                array_push($cleaned, $item);
            }
        }

        // 
        $cleaned = array_unique(array_column($data, 'url'));
        $cleaned =  array_intersect_key($data, $cleaned);


        if (config('extra.shuffle_data')) {
            shuffle($cleaned);
        }


        return $cleaned;
    }

    public function checkIfContainsDomainName($domain)
    {
        $pattern = '/^(http[s]?\:\/\/)?(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/';
        return preg_match($pattern, $domain);
    }

    /** fake save image **/
    public function image($query, $subquery)
    {
        $cache_key  = "search_" . md5($query);
        $output     = $this->cache->get($cache_key);

        if (!$output['data']) {
            if ($output['data']) {
                $this->wallpaper->random_key($cache_key);
                $this->cache->save($cache_key, $output, null, ['search']);
            }
        }

        $gallery              = getGallery($subquery, $output['data']);
        header('Content-type: image/jpeg');
        echo file_get_contents($gallery['current']['image']);
        exit();
    }

    /** halaman page **/
    public function page($pagename)
    {
        $data['random_terms'] = random_terms(30);
        $data['page_title']   = ucwords(clean($pagename));
        $data['path']         = __FUNCTION__; //page
        echo $this->blade->theme("page", $data);
    }

    /** halaman list_term **/
    public function list_term($abjad = 'a')
    {



        $term = random_terms(1000);

        $data['random_terms'] = get_alphabet($term, $abjad);

        $data['list_title']   = ucwords($abjad);
        $data['path']         = __FUNCTION__; //list_term
        echo $this->blade->theme("list", $data);
    }
}
