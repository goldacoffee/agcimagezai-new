<?php

error_reporting(E_ALL);
date_default_timezone_set("Asia/Jakarta");

define('BASE_PATH', $_SERVER['DOCUMENT_ROOT']);
define('APP_PATH',  BASE_PATH . '/app');

/**
 * Include Autoloader
 */
include APP_PATH . '/services/loader.php';

/**
 * Include Router
 */
include BASE_PATH . '/router.php';
